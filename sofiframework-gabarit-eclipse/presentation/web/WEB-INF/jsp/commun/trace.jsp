<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<sofi-html:text libelle="payment.label.common.createBy"
                property="creePar" disabled="true" size="25">
                &nbsp;<sofi-html:text property="dateCreation" disabled="true" />
</sofi-html:text>
<sofi-html:text libelle="payment.label.common.updateBy"
                property="modifiePar" disabled="true" size="25">
                &nbsp;<sofi-html:text property="dateModification" disabled="true" />
</sofi-html:text>