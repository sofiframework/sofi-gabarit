//Mettre les functions de l'organisations ici
/*
 * Permet de cocher tout ou de décocher tout sur 
 * un groupe de checkbox.
 * Aucun changement n'est effectuée si la case à  cocher a la propriété disabled Ã  true
 */
function cocherToutDecocherTout(checkbox, nameListeCheckbox) {
  var checks = $("input[name='" + nameListeCheckbox + "']:enabled");
  checks.attr("checked", !checkbox.checked);
  checks.trigger('click');
}

// Fonction permettant suivant la valeur de la case à  cocher et le groupe auquel elle fait partir
// d'activer / désactiver les boutons dont l'identifiant est passé dans le tableau en paramétre
function mettreAJourBoutons(checkbox, nameListeCheckbox, tabBouton) {
  // Par défaut, les boutons seront activés
  var attrDisabled = false;
  var attrClassName = 'bouton';
  
  // Il faut parcourir le groupe entier étre sér qu'il n'y pas pas d'autres cases cochées
  if(!checkbox.checked) {
    var checks = document.getElementsByName(nameListeCheckbox);
    var isUneCaseCochee = false;
    for(i = 0; i < checks.length && !isUneCaseCochee; i++) {
      if(checks[i].checked) {
        isUneCaseCochee = true;
      }
    }
    if(!isUneCaseCochee) {
      var attrDisabled = true;
      var attrClassName = 'boutondisabled';
    }
  }
  
  // Activation/désactivation des boutons
  for(var i = 0; i < tabBouton.length; i++) {
    var bouton = document.getElementById(tabBouton[i]);
    if(bouton!=null) {
      bouton.disabled = attrDisabled;
      bouton.className = attrClassName;
    }
  }
}

// Fonction permettant d'activer tous les boutons d'un formulaire
function activerBouton(theform) {
  if (theform && (document.all || document.getElementById)) {
    for (i = 0; i < theform.length; i++) {
      var tempobj = theform.elements[i];
      if (tempobj.type) {
        if (tempobj.type.toLowerCase() == "button" || tempobj.type.toLowerCase() == "reset" || tempobj.type.toLowerCase() == "submit") {
          tempobj.disabled = false;
          try {
            var classCourante = tempobj.className;
            if (classCourante.indexOf("disabled") != -1) {
              var indexDisabled = classCourante.indexOf("disabled");
              tempobj.className = classCourante.substr(0,indexDisabled);
            }
          } catch(E) {
            tempobj.className='bouton';
          }
        }
      }
    }
  }
}

/*
 * Permet d'activer ou de désactiver un bouton via 
 * son id en appliquant les bonnes classes css
 */
function changerEtatBouton(idBouton, actif) {
  if(actif) {
    $("#"+idBouton).removeAttr('disabled');
    $("#"+idBouton).removeClass('boutondisabled');
    $("#"+idBouton).addClass('bouton');
  } else {
    $("#"+idBouton).removeClass('bouton');
    $("#"+idBouton).addClass('boutondisabled');
    $("#"+idBouton).attr('disabled','true');
  }
}

/*
 * Permet de cacher ou afficher graduellement
 * une zone de texte.
 */
function inverserAffichage(toggleTag,
                           viewToChangeTag, 
                           viewToChangeTypeTag, 
                           viewToChangeClass){
  var style = getStyle(viewToChangeTag, "display");
  viewToChangeTypeTag = viewToChangeTypeTag + ".";
  
  //si le tag est non visible
  if(style == "none") {
    $(viewToChangeTypeTag + viewToChangeClass).slideDown("fast");
    toggleTag.innerHTML = '[&nbsp;-&nbsp;]&nbsp;';
  }
  //si le tag est visible
  else if (style == "block") {
    $(viewToChangeTypeTag + viewToChangeClass).slideUp("fast");
    toggleTag.innerHTML = '[&nbsp;+&nbsp;]&nbsp;';
  }
}

/*
 * Permet de tenir un nombre de caractère 
 * à jour pour un champ texte.
 */
function majNombreCaractere(champ, maxlength) {
  var champ = $(champ);
  var nbrRetourChariot = champ.attr('value').split(/\r?\n|\r/).length -1;  
  var nbrTotalCaractere = champ.attr('value').length + nbrRetourChariot;
  var compteur = champ.next('.compteur-caractere');
  compteur.html(nbrTotalCaractere + '/' + maxlength);

  if(nbrTotalCaractere > maxlength) {
    compteur.css('color', 'red');
  } else {
    compteur.css('color', 'black')
  }
}

//format :'yy-mm-dd'
function afficherCalendrier(composant, format) {
  $(composant).datepicker({
    
    dateFormat: format, 
    shortYearCutoff: '+10000', 
    changeMonth:true, 
    changeMonth:true,
    constrainInput:true,
    buttonImage : true,
    showButtonPanel:true
  });
  
  date = $(composant).val();
  if (date != '') {
    dateSlash = date.replace(/-/g, '/');
    date = Date.parse(dateSlash);
    date = new Date(date);
  }

  $(composant).datepicker('setDate', date);
  $(composant).datepicker('show');
}
