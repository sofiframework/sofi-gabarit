
function isNombre(input) {
   return (input - 0) == input && input.length > 0;
}

function toNumber(string) {
  return (string.split(' ').join('') - 0);
 }

function getCellule(x, y) {//lignes[1].cellules[0].montant 
  var cellule = $('#lignes\\[' + y + '\\]\\.cellules\\[' + x + '\\]\\.montant');
  return cellule;
}

function getTotalColonne(x) {
  var div = $('#totalColonne' + x);
  return div;
}

/* Fonction permettant d'ajouter des styles 
 * en over sur les pages du budget. Les pages
 * doivent appeler cette méthode sur le ready du document*/
appliquerStyleGrille = function(){
  $("input.saisie_alignement_droite").focus(function(){
    $(this).removeClass().addClass("saisie_alignement_droite saisieFocus");
  }).blur(function(){
    $(this).removeClass().addClass("saisie_alignement_droite");
  }).mouseover(function(){
    if (!$(this).hasClass("saisieFocus" )){$(this).removeClass().addClass("saisie_alignement_droite saisieOver");}
  }).mouseout(function(){
    if (!$(this).hasClass("saisieFocus" )){$(this).removeClass().addClass("saisie_alignement_droite");}
  }); 
}