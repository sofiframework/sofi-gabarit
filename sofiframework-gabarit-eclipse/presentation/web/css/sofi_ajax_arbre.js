function initArbre(id, urlChangement, urlSelection) {
  var conf = {
      data  : {
        type  : "json",
        async : true,
        url   : urlChangement,
        urlSelection : urlSelection
      },
      ui : {
        theme_name : "checkbox"
      },
      callback : {
        onchange : function (NODE, TREE_OBJ) {
          changementNoeud(NODE, TREE_OBJ);
          afficherMessageModificationArbre();
        },
        onopen : function (OBJ) {
          
          apresChargementArbre(id, OBJ);
        },
        onload : function (OBJ) {
          
          apresChargementArbre(id, OBJ);
        }
      }
    };
  
  var arbre = $.tree_create();
  arbre.init($("#" + id), $.extend({}, conf));
  initialiserMessageInformation();
}

function changementNoeud(NODE, TREE_OBJ) {
  if(TREE_OBJ.settings.ui.theme_name == "checkbox") {
    // Si cest un li, this est le li sinon cest le parent du noeud.
    var $this = $(NODE).is("li") ? $(NODE) : $(NODE).parent();

    // enlever les classes clicked sur les enfants
    $this.children("a").removeClass("clicked");

    // Si cochés
    if($this.children("a").hasClass("checked")) {
      // Décocher
      $this.find("li").andSelf().children("a").removeClass("checked").removeClass("undetermined").addClass("unchecked");
      var state = 0;
    } else {
      //Sinon cocher
      $this.find("li").andSelf().children("a").removeClass("unchecked").removeClass("undetermined").addClass("checked");
      var state = 1;
      if ($this.hasClass("closed")) {
        // Si on coche on veux aussi ouvrir le niveau enfant si il est fermé.
        TREE_OBJ.open_branch($this);
      }
    }
    
    // Trouver les valeurs modifiés
    var urlSelection = TREE_OBJ.settings.data.urlSelection;
    var listeId = '';
    
    $this.find("li").andSelf().each(function () {
      listeId += $(this).attr('id') + ',';
    });
    
    // pour toules les parents li
    $this.parents("li").each(function () {
      var parents = ajusterParentee(this, state)
      listeId += parents;
    });
    
    // Appeler l'url de mise à jour
    var action = (state == 1 ? 'selection' : 'deselection');
    
    $.get(urlSelection + '&action=' + action + '&listeId=' + listeId);
  }
}

function ajusterParentee(composant, state) {
  var listeId = '';

  // si cocher
  if(state == 1) {
    if($(composant).find("a.unchecked, a.undetermined").size() - 1 > 0) {
      $(composant).parents("li").andSelf().children("a").removeClass("unchecked").removeClass("checked").addClass("undetermined");
      // Ajouter les enlevés
      $(composant).parents("li").andSelf().each(function() {
        listeId += $(composant).attr('id') + ',';
      });
    } else {
      $(composant).children("a").removeClass("unchecked").removeClass("undetermined").addClass("checked");
      // Ajouter les cochés
      listeId += $(composant).attr('id') + ',';
    }
  // Si decocher
  } else {
    if($(composant).find("a.checked, a.undetermined").size() - 1 > 0) {
      $(composant).parents("li").andSelf().children("a").removeClass("unchecked").removeClass("checked").addClass("undetermined");
      // Ajouter les enlevés
      $(composant).parents("li").andSelf().each(function() {
        listeId += $(composant).attr('id') + ',';
      });
    } else {
      $(composant).children("a").removeClass("checked").removeClass("undetermined").addClass("unchecked");
      // Ajouter les non cochés
      listeId += $(composant).attr('id') + ',';
    }
  }

  return listeId;
}

function apresChargementArbre(id, OBJ) {
  var arbre = $.tree_reference(id);
  var composant = OBJ.container ? OBJ.container : $(OBJ);
  var listeLi = OBJ.container ? OBJ.container.find("li") : $(OBJ).find("li"); 
  
  var effectuerTraitement = OBJ.container != null;
  if (!effectuerTraitement) {
    effectuerTraitement = composant.find("li[nouveau = 'true']").length > 0;
  }
  
  if (effectuerTraitement) {
    // Mettre les styles unchecked partout pour commencer
    listeLi.each(function() {
      var checked = $(this).attr('checked');
      if (checked != 'true') {
        $(this).children('a').addClass('unchecked');
      }
    });

    // Mettre les classes checked ensuite
    listeLi.each(function() {
      var checked = $(this).attr('checked');
      if (checked == 'true') {
        $(this).children('a').addClass("checked");
        $(this).removeAttr('checked');
        $(this).parents("li").each(function() {
          ajusterParentee(this, 1);
        });
      }
    });

    // Vérifier les coches et les enfants pour les ouvrir aussi.
    // Donc si coché et closed on doit caller open.
    // on doit aussi cocher les enfants
    composant.find("a.checked").each(function() {
      $(this).parent("li").find("li").each(function(){
        if (!$(this).children("a").hasClass("checked")) {
          changementNoeud($(this), arbre);
        }
      });
    });
    
    composant.find("li[nouveau = 'true']").removeAttr('nouveau');
  }
}

/*
 * Ouvrir toutes les branches de l'arbre.
 */
function ouvrirArbre(id) {
  var arbre = $.tree_reference(id);
  arbre.container.find('li.closed').each(function() {
    arbre.open_branch($(this));
  });
}

function ouvrirSelectionArbre(id) {
  fermerArbre(id);
  var arbre = $.tree_reference(id);
  arbre.container.find('a.checked').parents('li').each(function() {
    arbre.open_branch($(this));
  });
}

function ouvrirArbreMultiple(id, nombreNiveau) {
  nombreNiveau = nombreNiveau == null ? 1 : nombreNiveau;
  for (var i = 0; i < nombreNiveau; i++) {
    setTimeout("ouvrirArbre('" + id + "');", (1000*i));
  }
}

function fermerArbre(id) {
  var arbre = $.tree_reference(id);
  arbre.container.find('li.open').each(function() {
    arbre.close_branch($(this));
  });
}

function cocherToutArbre(id) {
  var arbre = $.tree_reference(id);
  arbre.container.find("a.unchecked").each(function() {
    $(this).parent("li").find("li").each(function(){
      if (!$(this).children("a").hasClass("checked")) {
        changementNoeud($(this), arbre);
      }
    });
  });
}

function decocherToutArbre(id) {
  var arbre = $.tree_reference(id);

  arbre.container.find("a.checked").each(function() {
    $(this).parent("li").find("li").each(function(){
      if ($(this).children("a").hasClass("checked")) {
        changementNoeud($(this), arbre);
      }
    });
  });
}

function initDialogArbre(idDivArbre, idDivDialog, urlChargementArbre, urlSelectionArbre, urlChargementDialog, hauteur, largeur) {
  var fonctionArbre  = function (responseText, textStatus, XMLHttpRequest) {
    initArbre(idDivArbre, urlChargementArbre, urlSelectionArbre);
  }
  openDialog(idDivDialog, hauteur, largeur, true, 
      urlChargementDialog, fonctionArbre);
}

function initialiserMessageInformation(){  
  $("#" + 'messages_arbre').removeClass().addClass("zoneMessageFormulaireVide");
  $("#" + 'messageModificationArbre').removeClass().addClass("cacherDiv");
  $("#" + 'messageEnregistrementArbre').removeClass().addClass("cacherDiv");
  $("#" + 'spanMessageModificationArbre').removeClass();
  $("#" + 'spanMessageEnregistrementArbre').removeClass();
  
}

function afficherMessageModificationArbre(){ 
  initialiserMessageInformation();
  $("#" + 'messages_arbre').removeClass().addClass("zoneMessage");
  $("#" + 'messageModificationArbre').removeClass().addClass("afficherDiv");
  $("#" + 'spanMessageModificationArbre').removeClass().addClass("message_tout_type message_icone_enregistrement");  
  
}

function afficherMessageEnregistrementArbre(){  
  initialiserMessageInformation();
  $("#" + 'messages_arbre').removeClass().addClass("zoneMessage");
  $("#" + 'messageEnregistrementArbre').removeClass().addClass("afficherDiv");
  $("#" + 'spanMessageEnregistrementArbre').removeClass().addClass("message_tout_type message_icone_enregistrement");
}
