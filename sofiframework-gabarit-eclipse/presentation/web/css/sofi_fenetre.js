  
  var readSizeFromCookie = false; // Determines if size and position of windows should be set/retreved by use of cookie
  var windowMinSize = [80,30];  // Mininum width and height of windows.
  
  var moveCounter = -1; 
  var startEventPos = new Array();
  var startPosWindow = new Array();
  var startWindowSize = new Array();
  var initResizeCounter = -1; 
  var activeWindow = false;
  var activeWindowContent = false;  
  var windowSizeArray = new Array();
  var windowPositionArray = new Array();
  var currentZIndex = 10000;
  var windowStateArray = new Array(); // Minimized or maximized
  var activeWindowIframe = true;
  var divCounter = 0;
  var zIndexSet = false;
  var idOfNewWindow = false;
  var windowMapping = new Array();
  var fenetreCourante;
  var idBouton;
  var divPopup;
  var idChampSaisie;
  
  var MSIEWIN = (navigator.userAgent.indexOf('MSIE')>=0 && navigator.userAgent.indexOf('Win')>=0 && navigator.userAgent.toLowerCase().indexOf('opera')<0)?true:false;
  var opera = navigator.userAgent.toLowerCase().indexOf('opera')>=0?true:false;

  
  var requeteAjax = new Array();
      
  /*
  These cookie functions are downloaded from 
  http://www.mach5.com/support/analyzer/manual/html/General/CookiesJavaScript.htm
  */  
  function Get_Cookie(name) { 
     var start = document.cookie.indexOf(name+"="); 
     var len = start+name.length+1; 
     if ((!start) && (name != document.cookie.substring(0,name.length))) return null; 
     if (start == -1) return null; 
     var end = document.cookie.indexOf(";",len); 
     if (end == -1) end = document.cookie.length; 
     return unescape(document.cookie.substring(len,end)); 
  } 
  // This function has been slightly modified
  function Set_Cookie(name,value,expires,path,domain,secure) { 
    expires = expires * 60*60*24*1000;
    var today = new Date();
    var expires_date = new Date( today.getTime() + (expires) );
      var cookieString = name + "=" +escape(value) + 
         ( (expires) ? ";expires=" + expires_date.toGMTString() : "") + 
         ( (path) ? ";path=" + path : "") + 
         ( (domain) ? ";domain=" + domain : "") + 
         ( (secure) ? ";secure" : ""); 
      document.cookie = cookieString; 
  } 
      
  function cancelEvent()  {
    return (moveCounter==-1 && initResizeCounter==-1)?true:false;
  }
  function initMove(e)  {   
    if(document.all)e = event;
    moveCounter = 0;
    switchElement(false,this);
    startEventPos = [e.clientX,e.clientY];
    startPosWindow = [activeWindow.offsetLeft,activeWindow.offsetTop];
    startMove();
    if(!MSIEWIN)return false;
  
  }
  
  function startMove()  {
    if(moveCounter>=0 && moveCounter<=10){
      moveCounter++;
      setTimeout('startMove()',5);
    }
  }
  
  function stopMove(e)  {
    if(document.all)e = event;
    moveCounter=-1;
    initResizeCounter=-1;
    if(!activeWindow || !activeWindowContent)return;
    var state = '0';
    if(windowStateArray[activeWindow.id.replace(/[^0-9]/g,'')])state = '1';
    
    Set_Cookie(activeWindow.id + '_attr',activeWindow.style.left.replace('px','') + ',' + activeWindow.style.top.replace('px','') + ',' + activeWindow.style.width.replace('px','') + ',' + activeWindowContent.style.height.replace('px','') + ',' + activeWindow.style.zIndex + ',' + state,50);
  }
  
  function moveWindow(e)  {
    if(document.all)e = event;
    if(moveCounter>=10){
      activeWindow.style.left = startPosWindow[0] + e.clientX - startEventPos[0]  + 'px';
      activeWindow.style.top = startPosWindow[1] + e.clientY - startEventPos[1]  + 'px';
      
    } 
    if(initResizeCounter>=10){
      var newWidth = Math.max(windowMinSize[0],startWindowSize[0] + e.clientX - startEventPos[0]);
      var newHeight = Math.max(windowMinSize[1],startWindowSize[1] + e.clientY - startEventPos[1]);
      activeWindow.style.width =  newWidth + 'px';
      activeWindowContent.style.height = newHeight  + 'px';   
      
      if(MSIEWIN && activeWindowIframe){
        activeWindowIframe.style.width = (newWidth) + 'px'; 
        activeWindowIframe.style.height = (newHeight+20) + 'px';  
      }
        
      
    }
    if(!document.all)return false;
  }
  
  
  function initResizeWindow(e)  {
    if(document.all)e = event;
    initResizeCounter = 0;
    switchElement(false,document.getElementById('dhtml_goodies_id' + this.id.replace(/[^\d]/g,'')));

    startWindowSize = [activeWindowContent.offsetWidth,activeWindowContent.offsetHeight];
    startEventPos = [e.clientX,e.clientY];
    
    if(MSIEWIN)activeWindowIframe = activeWindow.getElementsByTagName('IFRAME')[0];
    startResizeWindow();
    return false;
      
  }
  
  function startResizeWindow()  {
    if(initResizeCounter>=0 && initResizeCounter<=10){
      initResizeCounter++;
      setTimeout('startResizeWindow()',5);
    }
  }
  
  function switchElement(e,inputElement)  {
    if(!inputElement)inputElement = this;
    var numericId = inputElement.id.replace(/[^0-9]/g,'');
    var state = '0';
    if(windowStateArray[numericId])state = '1';
      
    if(activeWindow && activeWindowContent){
      Set_Cookie(activeWindow.id + '_attr',activeWindow.style.left.replace('px','') + ',' + activeWindow.style.top.replace('px','') + ',' + activeWindow.style.width.replace('px','') + ',' + activeWindowContent.style.height.replace('px','') + ',' + activeWindow.style.zIndex + ',' + state,50);
  
    }
    currentZIndex = currentZIndex/1 + 1;
    activeWindow = document.getElementById('dhtml_goodies_id' + numericId); 
    activeWindow.style.zIndex = currentZIndex;
    activeWindowContent = document.getElementById('windowContent' + numericId);

    Set_Cookie(activeWindow.id + '_attr',activeWindow.style.left.replace('px','') + ',' + activeWindow.style.top.replace('px','') + ',' + activeWindow.style.width.replace('px','') + ',' + activeWindowContent.style.height.replace('px','') + ',' + activeWindow.style.zIndex + ',' + state,50);
  }
  
  function hideWindow() {
    windowPositionArray[divCounter] = new Array(); 
    ancienIdBouton = '';
    switchElement(false,document.getElementById('dhtml_goodies_id' + this.id.replace(/[^\d]/g,'')));  
    supprimerToutLesDiv();
    fenetreCourante = null;
    activeWindow.style.display='none';    
  }
  
  function minimizeWindow(e,inputObj) {
    if(!inputObj)inputObj = this;
    var numericID = inputObj.id.replace(/[^0-9]/g,'');
    switchElement(false,document.getElementById('dhtml_goodies_id' + numericID));
    var state;  

    if(inputObj.src.indexOf('fenetre_minimise')>=0){
      activeWindowContent.style.display='none';
      document.getElementById('resizeImage'+numericID).style.display='none';  
      inputObj.src = inputObj.src.replace('fenetre_minimise','fenetre_maximise');
      windowStateArray[numericID] = false;
      state = '0';    
    }else{    
      activeWindowContent.style.display='block';
      document.getElementById('resizeImage'+numericID).style.display='';
      inputObj.src = inputObj.src.replace('fenetre_maximise','fenetre_minimise');
      windowStateArray[numericID] = true;
      state = '1';
    }
    
    Set_Cookie(activeWindow.id + '_attr',activeWindow.style.left.replace('px','') + ',' + activeWindow.style.top.replace('px','') + ',' + activeWindow.style.width.replace('px','') + ',' + activeWindowContent.style.height.replace('px','') + ',' + activeWindow.style.zIndex + ',' + state,50);

  }
  
  function initWindows(e,divObj)  {
    var divs = document.getElementsByTagName('DIV');
    
    if(divObj){
      var tmpDivs = divObj.getElementsByTagName('DIV');
      var divs = new Array();
      divs[divs.length] = divObj;
      
      for(var no=0;no<tmpDivs.length;no++){
        divs[divs.length] = tmpDivs[no];
      }
    }
    
    for(var no=0;no<divs.length;no++) {

      if(divs[no].className=='dhtmlgoodies_window') { 
        if(MSIEWIN){
          var iframe = document.createElement('IFRAME');
          iframe.style.border='0px';
          iframe.frameborder=0;
          iframe.style.position = 'absolute';
          iframe.style.backgroundColor = '#FFFFFF';
          iframe.style.top = '0px';
          iframe.style.left = '0px';
          iframe.style.zIndex = 100;
        
          
          var subDiv = divs[no].getElementsByTagName('DIV')[0];
          divs[no].insertBefore(iframe,subDiv);
          
        }         
        if(divObj){
          divs[no].style.zIndex = currentZIndex;
          currentZIndex = currentZIndex /1 + 1;
        }
        
        divCounter = divCounter + 1;  
        if(divCounter==1)activeWindow = divs[no];   
        divs[no].id = 'dhtml_goodies_id' + divCounter;  
        divs[no].onmousedown = switchElement;
        if(readSizeFromCookie)var cookiePos = Get_Cookie(divs[no].id + '_attr') + ''; else cookiePos = '';
        if(divObj)cookiePos='';
        var cookieValues = new Array();
        
        if(cookiePos.indexOf(',')>0){
          cookieValues = cookiePos.split(',');
          if(!windowPositionArray[divCounter])windowPositionArray[divCounter] = new Array();
          windowPositionArray[divCounter][0] = Math.max(0,cookieValues[0]);
          windowPositionArray[divCounter][1] = Math.max(0,cookieValues[1]);
        }

        if(cookieValues.length==5 && !zIndexSet){
          divs[no].style.zIndex = cookieValues[4];
          if(cookieValues[4]/1 > currentZIndex)currentZIndex = cookieValues[4]/1;         
        }
        if(windowPositionArray[divCounter]){
          divs[no].style.left = windowPositionArray[divCounter][0] + 'px';  
          divs[no].style.top = windowPositionArray[divCounter][1] + 'px'; 
        }
        
        var subImages = divs[no].getElementsByTagName('IMG');
        for(var no2=0;no2<subImages.length;no2++){
          if(subImages[no2].className=='resizeImage'){
            subImages[no2].style.cursor = 'nw-resize';
            subImages[no2].onmousedown = initResizeWindow;
            subImages[no2].id = 'resizeImage' + divCounter;
            break;
          } 
          if(subImages[no2].className=='closeButton'){
            subImages[no2].id = 'closeImage' + divCounter;
            subImages[no2].onclick = hideWindow;  
          } 
          if(subImages[no2].className=='minimizeButton'){
            subImages[no2].id = 'minimizeImage' + divCounter;
            subImages[no2].onclick = minimizeWindow;  
            if(cookieValues.length==6 && cookieValues[5]=='0'){             
              setTimeout('minimizeWindow(false,document.getElementById("minimizeImage' + divCounter + '"))',10);
            }
            if(cookieValues.length==6 && cookieValues[5]=='1'){             
              windowStateArray[divCounter] = 1;
            } 
          }
        }     
      } 
      if(divs[no].className=='dhtmlgoodies_windowMiddle' || divs[no].className=='dhtmlgoodies_window_bottom'){
        divs[no].style.zIndex = 1000;
        
      }
      if(divs[no].className=='dhtmlgoodies_window_top'){
        divs[no].onmousedown = initMove;
        divs[no].id = 'top_bar'+divCounter;
        divs[no].style.zIndex = 1000;
  
      }
      
      if(divs[no].className=='dhtmlgoodies_windowContent'){
        
        divs[no].id = 'windowContent'+divCounter;
        divs[no].style.zIndex = 1000;
        if(cookieValues && cookieValues.length>3){
          if(!windowSizeArray[divCounter])windowSizeArray[divCounter] = new Array();
          windowSizeArray[divCounter][0] = cookieValues[2];
          windowSizeArray[divCounter][1] = cookieValues[3];
        } 
        if(cookieValues && cookieValues.length==5){
          activeWindowContent = document.getElementById('windowContent' + divCounter);  
        } 

        if(windowSizeArray[divCounter]){
          divs[no].style.height = windowSizeArray[divCounter][1] + 'px';
          divs[no].parentNode.parentNode.style.width = windowSizeArray[divCounter][0] + 'px';
          
          if(MSIEWIN){
            iframe.style.width = (windowSizeArray[divCounter][0]) + 'px';
            iframe.style.height = (windowSizeArray[divCounter][1]+20) + 'px';
          }
        }
      }
    } 
    
    if(!divObj){
      document.body.onmouseup = stopMove; 
      document.body.onmousemove = moveWindow;
      document.body.ondragstart = cancelEvent;
      document.body.onselectstart = cancelEvent;
    }
    fenetreCourante = document.getElementById('windowContent' + divCounter);
    return fenetreCourante;
  }
  
  function showAjaxContent(ajaxIndex, divPopup) {
    var response = requeteAjax[ajaxIndex].response;
    divPopup.innerHTML = response;
    activeWindow = document.getElementById('dhtml_goodies_id' + divCounter);  
    activeWindow.style.display='block';
  }
  
  function addContentFromUrl(url,divPopup) {
    //alert('url = ' + url);
    var ajaxIndex = requeteAjax.length;
    requeteAjax[ajaxIndex] = new sack();
    requeteAjax[ajaxIndex].requestFile = url; // Specifying which file to get
    requeteAjax[ajaxIndex].onCompletion = function(){ showAjaxContent(ajaxIndex, divPopup); };  // Specify function that will be executed after file has been found
    requeteAjax[ajaxIndex].runAJAX();   // Execute AJAX function      
  }
  
  function customFunctionCreateWindow(nomFenetre, urlToExternalFile,width,height,x,y,leftAjust,topAjust) {    
    divPopup = createNewWindow(nomFenetre, width,height,x,y,leftAjust,topAjust);
    windowMapping[nomFenetre] = divCounter;
    
    divPopup.innerHTML = '<br/><br/>Chargement de la page, un instant s.v.p.';
    if(urlToExternalFile)addContentFromUrl(urlToExternalFile + '&indexDivAJAX=' + divCounter, divPopup);  // Add content from external file
    

  }
  
  var ancienneValeur = '';
  var ancienIdBouton = '';
  
 
  function creerNouvelleFenetre(nomFenetre, idBouton, largeur, hauteur, ajustementX, ajustementY, e) {
        
    var bouton= document.getElementById(idBouton);

    var appelFenetre = true;

    try {
      var valeurCourante = bouton.value;

      if ( ancienIdBouton && (ancienIdBouton == idBouton)) {
        appelFenetre = false;
      }
            
      ancienneValeur = valeurCourante;
      ancienIdBouton = idBouton;
      
    }catch (E) {
    }
    
    if (appelFenetre) {
      var positionnerSelonBouton = (ajustementX != null && ajustementX != '0' && ajustementX != 'null')
      || (ajustementY != null && ajustementY != '0' && ajustementY != 'null');
      
      if (positionnerSelonBouton) {
          positionY = getPositionYBouton(bouton, ajustementY);
          positionX = getPositionXBouton(bouton, ajustementX);
      } else {
          positionX = getPositionX(largeur);
          positionY = getPositionY(hauteur);
      }
  
      if (windowMapping[nomFenetre] != undefined) {
        activeWindow.style.display='none';
      }
      
      var div = document.createElement('DIV');
      div.className='dhtmlgoodies_window';
      document.body.appendChild(div);
      
      var topDiv = document.createElement('DIV');
      topDiv.className='dhtmlgoodies_window_top';
      div.appendChild(topDiv);
      
      var buttonDiv = document.createElement('DIV');
      buttonDiv.className='top_buttons';
      topDiv.appendChild(buttonDiv);
      
      
      var img = document.createElement('IMG');
      img.src = 'images/sofi/fenetre_minimise.gif';
      img.className='minimizeButton';
      buttonDiv.appendChild(img); 
      
      var img = document.createElement('IMG');
      img.src = 'images/sofi/fenetre_quitter.gif';
      img.className='closeButton';
      buttonDiv.appendChild(img);     
            
      var middleDiv = document.createElement('DIV');
      middleDiv.className='dhtmlgoodies_windowMiddle';
      div.appendChild(middleDiv);
      
      var contentDiv = document.createElement('DIV');
      contentDiv.className='dhtmlgoodies_windowContent';
      middleDiv.appendChild(contentDiv);
      
      var bottomDiv = document.createElement('DIV');
      bottomDiv.className='dhtmlgoodies_window_bottom';
      div.appendChild(bottomDiv);
      /**/
      var img = document.createElement('IMG');
      img.src = 'images/sofi/fenetre_coin_droit.gif';
      img.className='resizeImage';
      bottomDiv.appendChild(img); 

      windowSizeArray[divCounter+1] = [largeur,hauteur];
      windowPositionArray[divCounter+1] = [positionX,positionY];
      fenetreCourante = initWindows(true,div);
      return fenetreCourante;
    }else {
      if (fenetreCourante && fenetreCourante != null) {
        return fenetreCourante;
      }
    }
  }  
  
  function getPositionXBouton(bouton, ajustementX) {
    var positionX = findPosX(bouton);
    positionX = parseInt(positionX) + parseInt(ajustementX);    

      if (positionX < 1) {
        positionX = 30;
      }
      
      return positionX;
  }

  function getPositionYBouton(bouton, ajustementY) {
      var positionY = findPosY(bouton);
      positionY = parseInt(positionY) + parseInt(ajustementY);
   
      if (!IE) {
        positionY = parseInt(positionY) +10;
      }
      
      return positionY;
  }
  
  function fermerFenetreAJAX(id) {
    fermerFenetreAjaxPourId(id);
  }  
  
  function fermerFenetreAjaxPourId(id) {
    fenetreCourante = null;
    fireOnClickEvent(id);
    NiceTitles.autoCreation();
  }
  
  /**
   Permet de fenetre la fenêtre présentement 
  */
  function fermerFenetreAjax() {
    fermerFenetreAjaxPourId('closeImage' + divCounter);
  }
    
  
  function supprimerToutLesDiv(){
  
    try {
      var oChild = activeWindow.firstChild;
  
      var nextChild;
  
      if(!oChild){ return; }
      while(oChild){
        nextChild = oChild.nextSibling;
        activeWindow.removeChild(oChild);
        oChild =  nextChild;
      }
    }catch(E) {
    }
  }
  
  function genererFenetreListeValeurAJAX(nomFenetre, idObjetPosition, idChampSaisie, url, largeur, hauteur, ajustementGauche, ajustementHaut, event, fonctionApresChargement) {

    divPopup = creerNouvelleFenetre(nomFenetre, idObjetPosition, largeur, hauteur, ajustementGauche, ajustementHaut, event);
    
       
    if (divPopup) {
      windowMapping[nomFenetre] = divCounter;

      if(url) {
        url = url + '&indexDivAJAX=' + divCounter;
        lancerAppelAJAX(url, divPopup, idChampSaisie, fonctionApresChargement);
      }
    }

  }
  
  
  function genererFenetreFlottanteAJAX(nomFenetre, idBouton, url, largeur, hauteur, ajustementGauche, ajustementHaut, event, fonctionApresChargement) {
    divPopup = creerNouvelleFenetre(nomFenetre, idBouton, largeur, hauteur, ajustementGauche, ajustementHaut, event);
    
    if (divPopup) {
      windowMapping[nomFenetre] = divCounter;

      if(url) {
        lancerAppelAJAX(url, divPopup, idChampSaisie, fonctionApresChargement);
      }
    }
  }  
  
  function lancerAppelAJAX(url, divPopup, idBouton, fonctionApresChargement) {
 
    var appelApresChargement = 'traiterReponseAJAX(indexRequeteAjax, divPopup, idBouton);'
    
    if (fonctionApresChargement) {
     appelApresChargement = appelApresChargement + fonctionApresChargement;
    }

    traiterAffichageDivAjaxAsynchrone(url, divPopup, null, appelApresChargement, null, false, false)
  }
  
  function traiterReponseAJAX(ajaxIndex, divPopup, idChamp) {
    var reponse = requeteAjax[ajaxIndex].response;
    if (reponse.indexOf('<div') != -1)  {
      divPopup.innerHTML = reponse;
      activeWindow = document.getElementById('dhtml_goodies_id' + divCounter);  
      activeWindow.style.display='block';
    } else if (reponse.indexOf('<message_erreur>') != -1) {
      var sourceXml = requeteAjax[ajaxIndex].responseXML.documentElement;
      afficherErreurChamps(sourceXml);
      fermerFenetreAjaxPourId('closeImage' + divCounter);    
    } else {
      var sourceXml = requeteAjax[ajaxIndex].responseXML.documentElement;
      populerChampsListeValeurAJAX(sourceXml);
      fermerFenetreAjaxPourId('closeImage' + divCounter);    
    }
    focus(idChamp);
    
    initialiserChampsPlaceholder();

  }
  
  function afficherErreurChamps(sourceXml) {
    // Accéder aux éléments de réponse.
    var message = sourceXml.getElementsByTagName("message_erreur")[0].firstChild.nodeValue;
    var champsEnErreur = sourceXml.getElementsByTagName("champ");
    var premierChampEnErreur = '';

    // Itérer chacun des éléments de réponse.
    for (var i = 0; i < champsEnErreur.length; i++) {
      var nom = champsEnErreur[i].firstChild.nodeValue;
      champ = document.getElementById(nom);
      if (premierChampEnErreur == '' && champ.type != "hidden") {
       premierChampEnErreur = nom;
      }

      appliquerStyleChampErreur(champ, message);
    }
    
    NiceTitles.autoCreation();
   
    
    document.getElementById(premierChampEnErreur).focus();   
    
    initialiserPlaceHolder();
  }
  
  function populerChampsListeValeurAJAX(sourceXml) {
    // Accéder aux éléments de réponse.
    var items = sourceXml.getElementsByTagName("item");

    // Itérer chacun des éléments de réponse.
    for (var i = 0; i < items.length; i++) {
      var nom = ""; 
      var valeur = "";

      try {
        nom = items[i].getElementsByTagName("nom")[0].firstChild.nodeValue;
        valeur = items[i].getElementsByTagName("valeur")[0].firstChild.nodeValue;

        champ = document.getElementById(nom);
        
        champ.value = valeur;

        appliquerStyleChampSaisie(champ);
        
      } catch (E) {
        element.value = "";
        alert('La propriété ' + nom + ' ne peut pas être populée.');
      }
    }
    
  }
  
  function mettreStyleErreur(e) {
    this.className='saisieErreur';
  }
  
  function mettreStyleSaisieFocus(e) {
    this.className='saisieFocus';
  } 
  
  function mettreStyleSaisieBlur(e) {
    this.className='saisieNormal';
  }  
  
  function appliquerStyleChampErreur(champ, message) {
	if ($(champ).attr('placeholder') == null) {
	    champ.className = 'saisieErreur';
	    champ.onfocus = mettreStyleErreur;
	    champ.onblur = mettreStyleErreur; 
	}
    champ.title = message;
    champ.setAttribute("nicetitle", message);
  }
  
  function appliquerStyleChampSaisie(champ) {
    try {
    	if ($(champ).attr('placeholder') == null) {
	      champ.className = 'saisieNormal';
	      champ.onfocus = mettreStyleSaisieFocus;
	      champ.onblur = mettreStyleSaisieBlur;
	      champ.title = '';
	      champ.setAttribute("nicetitle", '');
    }
    } catch(e) {
    }
  }
  
  function creerZoomTexte(nomChamp, largeur, hauteur, x, y, 
      cols, rows, maxlength, messageTexteTropLong, 
      libelleNombreCaractere, libelleBoutonModifier, libelleBoutonAnnuler, evenement) {
    var champ = document.getElementById(nomChamp);
    var nomFenetre = 'loupe_texte_' + nomChamp;
    var divPopup = creerNouvelleFenetre(nomFenetre, nomChamp, largeur, hauteur, x, y, evenement);
    
    var textarea = document.createElement('textarea');
    textarea.id = 'loupeTexte';
    textarea.name = 'loupeTexte';
    
    textarea.onchange = function () {
      eval(getContenuFonction(champ.onchange));
    };
    
    textarea.onfocus = function () {
      eval(getContenuFonction(champ.onfocus));
    };
    
    var onblur = getContenuFonction(champ.onblur).replace(nomChamp, 'loupeTexte');
    textarea.onblur = function (){
      eval(blur);
    };

    if (maxlength == null)  {
      textarea.onkeyup = function() {
        document.getElementById('compteur').innerHTML = 
          libelleNombreCaractere + this.value.length;
      };
    } else {
      textarea.onkeyup = function() {
        document.getElementById('compteur').innerHTML = 
          libelleNombreCaractere + this.value.length + '/' + maxlength;
      };
    }
    
    textarea.setAttribute('cols', cols);
    textarea.setAttribute('rows', rows);
    
    var divBoutons = document.createElement('div');
    divBoutons.id = 'boutonsPopup';
    divBoutons.className = 'barreBouton';
    
    var annuler = document.createElement('a');
    annuler.href = "javascript:fermerFenetreAJAX('closeImage" + divCounter + "');";
    annuler.className = "bouton";
    annuler.appendChild(document.createTextNode(libelleBoutonAnnuler));
  
    var modifier = document.createElement('a');
    modifier.href = "javascript:var champ = document.getElementById('" + nomChamp + 
      "');if (!alerteSiTexteTropLong(document.getElementById('loupeTexte'), '" + 
      messageTexteTropLong + "', " + maxlength + ")) { " + 
      " champ.value = document.getElementById('loupeTexte').value; " + 
      getContenuFonction(champ.onblur).replace('this', "document.getElementById('" + nomChamp + "')") + ";" + 
      getContenuFonction(champ.onkeyup).replace('this', "document.getElementById('" + nomChamp + "')") + ";" + 
      " champ.focus(); fermerFenetreAJAX('closeImage" + divCounter + "');}";
      
    modifier.className = "bouton";
    modifier.appendChild(document.createTextNode(libelleBoutonModifier));
    
    divBoutons.appendChild(annuler);
    divBoutons.appendChild(document.createTextNode(' '));
    divBoutons.appendChild(modifier);
  
    var compteur = document.createElement('div');
    compteur.setAttribute('id', 'compteur');
    compteur.appendChild(document.createTextNode(libelleNombreCaractere + champ.value.length));
    
    if (maxlength != null) {
      compteur.appendChild(document.createTextNode('/' + maxlength));    
    }
    
    divPopup.innerHTML = "";
    
    divPopup.appendChild(textarea);
    divPopup.appendChild(document.createElement('br'));
    divPopup.appendChild(document.createElement('br'));
    divPopup.appendChild(compteur);
    divPopup.appendChild(divBoutons);
    
    textarea.focus();
    
    var loupe = document.getElementById('loupeTexte');
    loupe.value = champ.value;
  }  
  
  function setTitreFenetre(titreFenetre) {

    var titreValue = $('#'+ 'titreFenetre' + divCounter).html();
    
    if (!titreValue) {
        var titleNode = document.createElement('div');
	    titleNode.innerHTML = "<div id='titreFenetre" + divCounter +"' class='titreFenetre'>" + titreFenetre + "</div>";
	    
	    var titleZone =  getId('top_bar' + divCounter);           
	    titleZone.insertBefore(titleNode,titleZone.firstChild);  
    }
  
  }
  
