package org.sofiframework.gabarit.presentation;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.gabarit.modele.ModeleGabarit;
import org.sofiframework.presentation.struts.controleur.BaseDispatchAction;

public abstract class GabaritAction extends BaseDispatchAction {

  protected ModeleGabarit getModeleGabarit(HttpServletRequest request) {
    return (ModeleGabarit) getModele(request);
  }
}
