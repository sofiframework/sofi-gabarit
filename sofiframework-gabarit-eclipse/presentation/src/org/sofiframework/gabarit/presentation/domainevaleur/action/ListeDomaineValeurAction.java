package org.sofiframework.gabarit.presentation.domainevaleur.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.domainevaleur.GestionDomaineValeur;
import org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur;
import org.sofiframework.application.domainevaleur.service.ServiceDomaineValeur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.presentation.struts.controleur.BaseDispatchAction;
import org.sofiframework.utilitaire.UtilitaireString;

public class ListeDomaineValeurAction extends BaseDispatchAction {

  public ActionForward rechercher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

    ListeNavigation liste = this.appliquerListeNavigation(request);
    liste.ajouterTriInitial("description", true);

    String domaine = request.getParameter("domaine");
    String description = request.getParameter("description");
    String codeLangue = getLangueUtilisateur(request);
    ServiceDomaineValeur serviceDomaineValeur = (ServiceDomaineValeur) this
        .getBean("serviceDomaineValeur", request);
    FiltreDomaineValeur filtre = (FiltreDomaineValeur) request.getSession()
        .getAttribute("filtreDomaineValeur");

    if (filtre == null || this.isInitialiserListeValeur(request)) {
      filtre = new FiltreDomaineValeur();
      filtre.setNom(domaine);
      if (!UtilitaireString.isVide(description)) {
        filtre.setDescription(description);
      }
      filtre.setCodeLangue(codeLangue);
      filtre.setRechercheApresAttribut(true);
    }
    liste.setFiltre(filtre);
    liste = serviceDomaineValeur.getListeValeur(liste);

    this
        .setAttributTemporairePourAction("filtreDomaineValeur", filtre, request);
    this.setAttributTemporairePourAction("listeDomaineValeur", liste, request);

    return genererReponseListeValeurAjax("ajax_liste_navigation", new String[] {
        "valeur", "description" },
        "eregroupement.erreur.liste_domaine_valeur.domaine_valeur.inexistant",
        liste, mapping, form, request, response);
  }

  private String getLangueUtilisateur(HttpServletRequest request) {
    return this.getLocale(request).getLanguage();
  }

  private ServiceDomaineValeur getBean(String string, HttpServletRequest request) {
    return GestionDomaineValeur.getInstance().getServiceDomaineValeur();
  }

}
