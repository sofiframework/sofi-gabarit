package org.sofiframework.gabarit.presentation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.presentation.struts.controleur.spring.BaseTilesRequestProcessor;

/**
 * Controleur de base pour effectuer des traitements avant ou apres toutes les
 * actions de l'application.
 * 
 * @author Jean-Maxime Pelletier
 * @author Jean-François Brassard
 */
public class GabaritRequestProcessor extends BaseTilesRequestProcessor {

  private static final Log log = LogFactory
      .getLog(GabaritRequestProcessor.class);

  @Override
  protected ActionForward traitementAvantAction(HttpServletRequest request,
      HttpServletResponse response, Action action, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException, Exception {
    
    String appStartDate = GestionSurveillance.getInstance().getDateDepartApplication();
    if (getServletContext().getAttribute("appStartDate") == null) {
      getServletContext().setAttribute("appStartDate", appStartDate);
    }
    return super
        .traitementAvantAction(request, response, action, form, mapping);
  }

  @Override
  public void process(HttpServletRequest arg0, HttpServletResponse arg1)
      throws IOException, ServletException {
    
	  long debut = System.currentTimeMillis();
    super.process(arg0, arg1);
    long fin = System.currentTimeMillis();

    if (log.isInfoEnabled()) {
      log.info("Struts temps process : " + (fin - debut));
    }
  }

  @Override
  protected ActionForward processActionPerform(HttpServletRequest request,
      HttpServletResponse response, Action action, ActionForm form,
      ActionMapping mapping) throws IOException, ServletException {
    long debut = System.currentTimeMillis();
    ActionForward forward = super.processActionPerform(request, response,
        action, form, mapping);
    long fin = System.currentTimeMillis();
    if (log.isInfoEnabled()) {
      log.info("Struts temps processActionPerform : " + (fin - debut));
    }
    return forward;
  }
}
