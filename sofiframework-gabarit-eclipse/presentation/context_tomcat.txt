
    <Resource name="jdbc/applicationDS" 
              auth="Container"
              type="javax.sql.DataSource"
              username="USER"
              password="MP"
              driverClassName="oracle.jdbc.driver.OracleDriver"
              url="jdbc:oracle:thin:@IP:PORT:SID"
              maxActive="100"
              maxIdle="4"
              testOnBorrow="true"
              validationQuery="select 1 from dual" />

    <Environment 
      name="infrastructure.url" 
      type="java.lang.String" 
      override="false"
      value="http://www.sofiframework.org/sofi-ws/distant" />

    <Environment 
      name="hibernate.show_sql" 
      type="java.lang.String" 
      override="false"
      value="false" />
      
    <Environment 
      name="hibernate.dialect" 
      type="java.lang.String" 
      override="false"
      value="org.hibernate.dialect.HSQLDialect" />
      
   
