package org.sofiframework.gabarit.modele.facette.service;

import org.sofiframework.modele.Service;

/**
 * Service Exemple
 * <p>
 * @author Jean-Francois Brassard, Nurun inc.
 * @version 1.0
 */
public interface ServiceExemple extends Service {
  /**
   * Retourne la  liste exemple du service exemple du gabarit.
   * @return la  liste exemple du service exemple du gabarit.
   */
  public Long[] getListeExemple();

}
