package org.sofiframework.gabarit.modele.facette.service.test.distant;

import org.sofiframework.gabarit.modele.facette.service.ServiceExemple;
import org.sofiframework.gabarit.modele.test.BaseTestGabaritDistant;


/**
 * Les cas d'essais sur le service exemple.
 * <p>
 *
 * @author Jean-Francois Brassard
 */
public class ServiceExempleDistantTest extends BaseTestGabaritDistant {
  /**
   * Test du service distant exemple
   */
  public void testgetListeExemple() {
    // Accès au service.
    ServiceExemple service = getServiceExemple();

    // Liste exemple
    Long[] listeExemple = service.getListeExemple();

    // Les cas d'essais.
    assertNotNull(listeExemple);
  }

}
