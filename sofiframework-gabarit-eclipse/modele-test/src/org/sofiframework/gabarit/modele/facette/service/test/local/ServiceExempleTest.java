package org.sofiframework.gabarit.modele.facette.service.test.local;

import org.sofiframework.gabarit.modele.facette.service.ServiceExemple;
import org.sofiframework.gabarit.modele.test.BaseTestGabarit;


/**
 * Les cas d'essais sur le service exemple.
 * <p>
 *
 * @author Jean-Francois Brassard
 */
public class ServiceExempleTest extends BaseTestGabarit {

  /**
   * Test du service distant exemple
   */
  public void testgetListeExemple() {
    // Accès au service.
    ServiceExemple service = getServiceExemple();

    // Liste exemple
    Long[] listeExemple = service.getListeExemple();

    // Les cas d'essais.
    assertNotNull(listeExemple);
  }


}
