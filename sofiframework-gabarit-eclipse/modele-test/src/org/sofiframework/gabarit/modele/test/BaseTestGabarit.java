package org.sofiframework.gabarit.modele.test;

import org.sofiframework.gabarit.modele.facette.service.ServiceExemple;
import org.sofiframework.modele.spring.junit.AbstractSpringTest;


/**
 * Classe de base d'essai unitaire pour Gabarit.
 * @version 1.0
 * @author
 */
abstract public class BaseTestGabarit extends AbstractSpringTest {
  public BaseTestGabarit() {
    super("baseTestGabarit");
  }

  /**
   * Fichier de configuration Spring pour la classe de tests.
   * @return les fichiers de configuration necessaire.
   */
 protected String[] getConfigurationsSpring() {
    return new String[] {
      "/org/sofiframework/gabarit/modele/conf/bd/hsql.xml",
      "/org/sofiframework/gabarit/modele/conf/entite.xml",
      "/org/sofiframework/gabarit/modele/conf/dao.xml",
      "/org/sofiframework/gabarit/modele/conf/service.xml",            
      "/org/sofiframework/gabarit/modele/conf/contexte-junit.xml",
      "/org/sofiframework/gabarit/modele/conf/service-distant-sofi-http.xml",
      "/org/sofiframework/gabarit/modele/conf/contexte.xml"
    };
  }

  /**
   * Acces au service.
   * @return le service.
   */
  protected ServiceExemple getServiceExemple() {
    return (ServiceExemple) this.getContexte().getBean("ServiceExempleBean");
  }
  
}
