Important :
  - Ouvrir le fichier init.bat dans un �diteur (ex. Bloc-notes)
  - V�rifier que l'emplacement de la JDK et de Ant soit valide sur votre poste.

Veuillez �diter le fichier init.properties et sp�cifiez les valeurs suivantes :

  code.application.nouveau = XXX
  code.application.nouveau_min = xxx
  titre.application.nouveau = Titre ou acronyme de l'application
  code.utilisateur.test = un code utilisateur d'essai pour authentifier dans l'application
  id.referentiel.accueil = l'identifiant du composant de la page d'accueil
