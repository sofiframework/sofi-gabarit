Important :
  - Ouvrir le fichier config.bat (ex. Bloc-notes)
  - Verifier que l'emplacement de la JDK et de Ant soit valide sur votre poste.

Ensuite, il est possible de double-cliquer sur un fichier qui va automatiquement lancer une tache Ant.
Le resultat de la tache sera ecrit dans un fichier de log correspondant a la tache (../log/tache.txt)

Description des taches offertes :

- rebuild
  - Permet de construire votre projet a neuf avec les librairies les plus � jour specifique 
    a votre configuration IVY (ivy.xml)
- compile
  - Permet une compilation de votre projet
- clean
  - Permet de nettoyer votre projet pour ensuite permettre une nouvelle compilation � neuf
- test
  - Permet d'executer tous les essais unitaires developpe dans votre espace de travail (workspace).
- war
  - Permet de g�n�rer un fichier de deploiement.
