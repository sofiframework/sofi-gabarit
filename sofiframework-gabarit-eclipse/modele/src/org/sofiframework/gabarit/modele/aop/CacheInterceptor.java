package org.sofiframework.gabarit.modele.aop;

import java.text.MessageFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;

import org.sofiframework.modele.spring.cache.service.ServiceCache;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireObjet;

public class CacheInterceptor {

  private static final Log log = LogFactory.getLog(CacheInterceptor.class);

  private ServiceCache serviceCache;

  public Object invoke(ProceedingJoinPoint pjp) throws Throwable {
    String id = this.getIdentifiantAppel(pjp);
    Object retour = null;
    long debut = System.currentTimeMillis();
    boolean enCache = false;
    if (this.serviceCache.isEnCache(id)) {
      enCache = true;
      retour = this.serviceCache.getValeur(id);
    } else {
      retour = pjp.proceed();
      this.serviceCache.ajouter(id, retour);
    }

    if (log.isInfoEnabled()) {
      log.info(MessageFormat.format("Appel {0} {1}", new Object[] {
        id, (enCache ? " Cache " : " NoCache ")
      }));
    }

    return retour;
  }

  /**
   * Obtenir l'identifiant unique de l'appel de la méthode.
   * 
   * @param invocationMethode
   *          invocation d'une méthode
   * @return identifiant unique
   */
  private String getIdentifiantAppel(ProceedingJoinPoint pjp) {
   String signature = pjp.getSignature().toLongString();
    Object[] arguments = pjp.getArgs();
    StringBuffer valeurArguments = new StringBuffer();

    for (int i = 0; i < arguments.length; i++) {
      Object argument = arguments[i];
      valeurArguments.append("[");
      if (argument != null) {
        if (argument instanceof ObjetTransfert) {
          String valeurCle = this.getValeurCle((ObjetTransfert) argument);
          valeurArguments.append(valeurCle);
        } else {
          valeurArguments.append(argument.toString());
        }
      } else {
        valeurArguments.append("null");
      }
      valeurArguments.append("]");
    }

    String identifiantAppel = MessageFormat.format(
        "signature = [{0}] arguments = [{1}]", new Object[] {signature, valeurArguments.toString() });

    return identifiantAppel;
  }

  /**
   * Obtenir la valeur en string de la clé d'un objet de transfert.
   * 
   * @param objet
   *          Objet de transfert
   * @return valeur de la clé
   */
  private String getValeurCle(ObjetTransfert objet) {
    StringBuffer cle = new StringBuffer();
    String[] attributsCle = objet.getCle();

    for (int i = 0; i < attributsCle.length; i++) {
      String attributCle = attributsCle[i];
      cle.append(UtilitaireObjet.getValeurAttribut(objet, attributCle));
    }

    return cle.toString();
  }

  public ServiceCache getServiceCache() {
    return serviceCache;
  }

  public void setServiceCache(ServiceCache serviceCache) {
    this.serviceCache = serviceCache;
  }

}
