# Gabarit de développement SOFI #
Projet offrant les différents gabarit de développement offert par la solution SOFI Framework. 

# Solution disponibles #

Vous pouvez choisir entre 2 solutions. L'une avec les technologies d'Oracle avec JDeveloper et l'autre 100% logiciel libre avec Eclipse. 

* sofiframework-gabarit-eclipse
* sofiframework-gabarit-jdev

# Configuration du projet #

Éditer le fichier init.properties qui se trouve dans /ant/init du gabarit Eclipse ou JDeveloper.

Saisir les valeurs selon le projet a créer. Voici les propriétés qui doivent être modifiés :

* type.organisation
* prefix.organisation
* infrastructure.url.nouveau
* code.application.nouveau
* code.application.nouveau.maj
* code.application.nouveau.min
* code.utilisateur.test
* id.referentiel.accueil