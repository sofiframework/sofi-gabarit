package org.sofiframework.gabarit.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.sofiframework.gabarit.facette.service.ServiceExemple;
import org.sofiframework.modele.adf.junit.BaseTestCase;

/**
 * Classe de base pour la configuration des tests Junit pour les services
 * local avec ADF.
 * <p>
 *
 * @author Jean-Francois Brassard
 */
public class BaseGabaritTest extends BaseTestCase {

  protected static final Log log = LogFactory.getLog(BaseGabaritTest.class);

  /**
   * Méthode d'accès au service exemple.
   * @return le service exemple.
   */
  protected ServiceExemple getServiceExemple() {
    return (ServiceExemple)this.getModele(ServiceExemple.class, "ServiceExempleLocal");
  }

}
