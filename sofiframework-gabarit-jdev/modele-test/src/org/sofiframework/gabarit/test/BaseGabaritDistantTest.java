package org.sofiframework.gabarit.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.sofiframework.gabarit.facette.service.ServiceExemple;
import org.sofiframework.modele.spring.junit.AbstractSpringTest;

/**
 * Classe de base pour la configuration des tests Junit pour les services
 * distant de Gabarit.
 * <p>
 *
 * @author Jean-Francois Brassard
 */
public class BaseGabaritDistantTest extends AbstractSpringTest {

  protected static final Log log = LogFactory.getLog(BaseFormationDistantTest.class);

  public BaseGabaritDistantTest() {
    super("baseGabaritDistantTest");
  }
   
  /**
   * Fichier de configuration Spring pour la classe de tests.
   * 
   * @return les fichiers de configuration nécessaire.
   */
  protected String[] getConfigurationsSpring() {

  	return new String[] {
          "/org/sofiframework/gabarit/modele/conf/contexte-junit.xml",
          "/org/sofiframework/gabarit/modele/conf/service-commun.xml",
          "/org/sofiframework/gabarit/modele/conf/contexte-commun.xml",
          "/org/sofiframework/gabarit/modele/conf/service-distant-gabarit-http.xml",};
  }

  /**
   * Accès au service exemple.
   * 
   * @return le service exemple
   */
  protected ServiceExemple getServiceExemple() {
    return (ServiceExemple) this.getContexte().getBean(
        "serviceExemple");
  }
 
}
