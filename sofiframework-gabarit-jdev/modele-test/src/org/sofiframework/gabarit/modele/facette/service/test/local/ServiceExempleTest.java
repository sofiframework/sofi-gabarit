package org.sofiframework.gabarit.facette.service.test.distant;

import org.sofiframework.gabarit.facette.service.ServiceExemple;
import org.sofiframework.gabarit.test.BaseGabaritTest;


/**
 * Les cas d'essais sur le service exemple.
 * <p>
 *
 * @author Jean-Francois Brassard
 */
public class ServiceExempleTest extends BaseGabaritTest {

  /**
   * Test du service distant exemple
   */
  public void testgetListeExemple() {
    // Accès au service.
    ServiceExemple service = getServiceExemple();

    // Liste exemple
    Long[] listeExemple = service.getListeExemple();

    // Les cas d'essais.
    assertNotNull(listeExemple);
  }


}
