package org.sofiframework.gabarit.modele.facette.service.test.distant;

import org.sofiframework.gabarit.facette.service.ServiceExemple;
import org.sofiframework.gabarit.test.BaseGabaritDistantTest;


/**
 * Les cas d'essais sur le service exemple.
 * <p>
 *
 * @author Jean-Francois Brassard
 */
public class ServiceExempleDistantTest extends BaseGabaritDistantTest {
  /**
   * Test du service distant exemple
   */
  public void testgetListeExemple() {
    // Accès au service.
    ServiceExemple service = getServiceExemple();

    // Liste exemple
    Long[] listeExemple = service.getListeExemple();

    // Les cas d'essais.
    assertNotNull(listeExemple);
  }

}
