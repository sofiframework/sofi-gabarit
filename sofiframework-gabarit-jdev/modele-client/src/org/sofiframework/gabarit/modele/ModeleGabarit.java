package org.sofiframework.gabarit.modele;

import org.sofiframework.modele.Modele;


/**
 * Le modele de l'application Gabarit.
 * @author
 */
public interface ModeleGabarit extends Modele {

}
