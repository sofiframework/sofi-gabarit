package org.sofiframework.gabarit.presentation;


import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.presentation.struts.controleur.spring.BaseTilesRequestProcessor;

/**
 * Controleur de base pour effectuer des traitements avant et/ou apres les actions de l'application.
 *
 * @author Jean-Francois Brassard
 * @version 1.0
 */
public class GabaritRequestProcessor extends BaseTilesRequestProcessor {

  /**
   * Methode pour les traitements qui doivent etre effectues avant chacune des actions.
   *
   * @throws java.lang.Exception Le detail de l'exception de base s'il y a lieu.
   * @throws javax.servlet.ServletException le detail de l'exception Servlet s'il y lieu.
   * @throws java.io.IOException le detail de l'exception I/O s'il y a lieu.
   * @return null
   * @param mapping les informations de correspondance Struts
   * @param form Le formulaire en cours d'utilisation.
   * @param action L'action Struts en traitement.
   * @param response La reponse en traitement.
   * @param request La requete en traitement.

   */
  protected ActionForward traitementAvantAction(HttpServletRequest request,
    HttpServletResponse response, Action action, ActionForm form,
    ActionMapping mapping) throws IOException, ServletException, Exception {
    // Inserer traitement personnalise ici.
    return super.traitementAvantAction(request, response, action, form, mapping);
  }

  /**
   *  Methode pour les traitements qui doivent etre effectues apres chacune des actions.
   *
   * @throws java.lang.Exception Le detail de l'exception de base s'il y a lieu.
   * @throws javax.servlet.ServletException le detail de l'exception Servlet s'il y lieu.
   * @throws java.io.IOException le detail de l'exception I/O s'il y a lieu.
   * @return null
   * @param mapping les informations de correspondance Struts
   * @param form Le formulaire en cours d'utilisation.
   * @param action L'action Struts en traitement.
   * @param response La reponse en traitement.
   * @param request La requete en traitement.

   */
  protected ActionForward traitementApresAction(HttpServletRequest request,
    HttpServletResponse response, Action action, ActionForm form,
    ActionMapping mapping) throws IOException, ServletException, Exception {
    // Inserer traitement personnalise ici.
    return super.traitementApresAction(request, response, action, form, mapping);
  }
}
