package org.sofiframework.gabarit.presentation.accueil.action;

import org.sofiframework.presentation.struts.controleur.BaseDispatchAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.presentation.struts.controleur.BaseDispatchAction;


/**
 * Action permettant les traitements d'interaction sur
 * le service d'accueil.
 *
 * @author Jean-Francois Brassard
 * @version 1.0
 */
public class AccueilAction extends BaseDispatchAction {
  /**
   * Methode d'acces initial au service d'accueil.
   *
   * @throws java.lang.Exception le detail de l'exception s'il y a lieu.
   * @return L'appel de la vue ou redirection a appliquer.
   * @param response La reponse en traitement.
   * @param request La requete en traitement.
   * @param form Le formulaire en cours d'utilisation.
   * @param mapping les informations de correspondance Struts
   */
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
    HttpServletRequest request, HttpServletResponse response)
    throws Exception {
    return mapping.findForward("afficher");
  }

  /**
   * Methode permettant l'affichage de la page d'accueil.
   *
   * @throws java.lang.Exception le detail de l'exception s'il y a lieu.
   * @return L'appel de la vue ou redirection a appliquer.
   * @param response La reponse en traitement.
   * @param request La requete en traitement.
   * @param form Le formulaire en cours d'utilisation.
   * @param mapping les informations de correspondance Struts
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
    HttpServletRequest request, HttpServletResponse response)
    throws Exception {
      // Appel de la page d'accueil.
      return mapping.findForward("page");
  }
}
