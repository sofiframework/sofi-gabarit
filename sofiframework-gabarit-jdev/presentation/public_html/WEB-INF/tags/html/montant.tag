<%@ tag body-content="scriptless" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>

<%@ attribute name="property" required="true" type="java.lang.String"%>
<%@ attribute name="nested" required="false" type="java.lang.Boolean"%>
<%@ attribute name="afficherDevise" required="false" type="java.lang.Boolean"%>
<%@ attribute name="obligatoire" required="false" type="java.lang.Boolean"%>
<%@ attribute name="format" required="false" type="java.lang.String"%>
<%@ attribute name="onChange" required="false" type="java.lang.String"%>
<%@ attribute name="styleClass" required="false" type="java.lang.String"%>

<c:set var="nested" value="${(nested eq null) ? false : nested }" />
<c:set var="afficherDevise" value="${(afficherDevise eq null) ? true : afficherDevise }" />
<c:set var="obligatoire" value="${(obligatoire eq null) ? false : obligatoire }" />
<c:set var="format" value="${(format eq null) ? '9.99' : format }" />

<c:choose>
  <c:when test="${nested == false}">
    <sofi-html:text property="${property}"  
                    maxlength="10"
                    formatterEnNombre="true" nombreNegatifActif="false"
                    nombreDefautZero="true"
                    format="${format}"
                    obligatoire="${obligatoire}"
                    onchange="${onChange}"
                    size="10"
                    styleClass="${styleClass}" >
      <c:if test="${afficherDevise == true}">
        <span class="texteFormat filnavigationsection">&nbsp;($ CAN)</span>
      </c:if>
    </sofi-html:text>
  </c:when>
  <c:otherwise>
    <sofi-nested:text property="${property}"  
                      maxlength="10"
                      formatterEnNombre="true" nombreNegatifActif="false"
                      nombreDefautZero="true"
                      format="${format}"
                      obligatoire="${obligatoire}"
                      onchange="${onChange}"
                      size="10" 
                      styleClass="${styleClass}">
      <c:if test="${afficherDevise == true}">
        <span class="texteFormat filnavigationsection">&nbsp;($ CAN)</span>
      </c:if>
    </sofi-nested:text>  
  </c:otherwise>
</c:choose>