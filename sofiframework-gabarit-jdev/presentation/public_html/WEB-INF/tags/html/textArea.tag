<%@ tag body-content="scriptless" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>

<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>

<%@ attribute name="property" required="true" type="java.lang.String"%>
<%@ attribute name="maxlength" required="false" type="java.lang.Integer"%>
<%@ attribute name="obligatoire" required="false" type="java.lang.Boolean"%>
<%@ attribute name="affichageIndObligatoire" required="false" type="java.lang.Boolean"%>
<%@ attribute name="affichageSeulement" required="false" type="java.lang.Boolean"%>
<%@ attribute name="rows" required="false" type="java.lang.Integer"%>
<%@ attribute name="cols" required="false" type="java.lang.Integer"%>
<%@ attribute name="styleClass" required="false" type="java.lang.String"%>
<%@ attribute name="nested" required="false" type="java.lang.String"%>

<c:set var="obligatoire" value="${(obligatoire eq null) ? false : obligatoire }" />
<c:set var="affichageIndObligatoire" value="${(affichageIndObligatoire eq null) ? false : affichageIndObligatoire }" />
<c:set var="affichageSeulement" value="${affichageSeulement}" />
<c:set var="rows" value="${(rows eq null or rows eq '') ? 3 : rows}" />
<c:set var="cols" value="${(cols eq null or cols eq '') ? 50 : cols }" />
<c:set var="maxlength" value="${(maxlength eq null) ? 150 : maxlength}" />
<c:set var="nested" value="${(nested eq null or nested eq '') ? false : nested }" />

<c:choose>
  <c:when test="${nested == false}">
  <sofi-html:textarea styleClass="${styleClass}" 
                      property="${property}" 
                      rows="${rows}" cols="${cols}" 
                      affichageSeulement="${affichageSeulement}"
                      onkeyup="majNombreCaractere(this, '${maxlength}');"
                      obligatoire="${obligatoire}" />
  </c:when>
  <c:otherwise>
    <sofi-nested:textarea styleClass="${styleClass}"
                          property="${property}" 
                          rows="${rows}" cols="${cols}" 
                          onkeyup="majNombreCaractere(this, '${maxlength}');"
                          obligatoire="${obligatoire}" />
  </c:otherwise>
</c:choose>
<app:nombreCaractere property="${property}" maxlength="${maxlength}" />
