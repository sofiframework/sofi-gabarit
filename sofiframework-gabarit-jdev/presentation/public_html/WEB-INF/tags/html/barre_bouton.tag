<%@ tag body-content="scriptless" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ attribute name="libelle_enregistrer" required="true" type="java.lang.String"%>
<%@ attribute name="libelle_retour" required="true" type="java.lang.String"%>
<%@ attribute name="libelle_ajouter" required="true" type="java.lang.String"%>
<%@ attribute name="libelle_supprimer" required="true" type="java.lang.String"%>
<%@ attribute name="url_retour" required="true" type="java.lang.String"%>
<%@ attribute name="url_ajouter" required="true" type="java.lang.String"%>
<%@ attribute name="url_supprimer" required="true" type="java.lang.String"%>
<%@ attribute name="message_suppression" required="false" type="java.lang.String"%>
<%@ attribute name="traitement_enregistrer" required="false" type="java.lang.String"%>
<%@ attribute name="disabled_condition" required="true" type="java.lang.Boolean"%>
<%@ attribute name="disabled_condition_enregistrer" required="false" type="java.lang.Boolean"%>
<%@ attribute name="disabled_condition_nouveau" required="false" type="java.lang.Boolean"%>
<%@ attribute name="disabled_condition_supprimer" required="false" type="java.lang.Boolean"%>

<div class="bouton-actions">
  <c:choose>
    <c:when test="${traitement_enregistrer ne null && traitement_enregistrer ne ''}">
      <sofi-html:bouton libelle="${libelle_enregistrer}" onclick="${traitement_enregistrer}" disabled="${disabled_condition_enregistrer ne null && disabled_condition_enregistrer}" />
    </c:when>
    <c:otherwise>
      <sofi-html:submit libelle="${libelle_enregistrer}" disabled="${disabled_condition_enregistrer ne null && disabled_condition_enregistrer}" />
    </c:otherwise>
  </c:choose>
  <!-- Possibilite d'ajouter d'autres boutons   -->
  <jsp:doBody />
  <c:if test="${libelle_retour ne null && libelle_retour ne ''}">
    <c:choose>
      <c:when test="${barre_haut}">
        <sofi-html:bouton styleId="btRetourHaut" 
                          href="${url_retour}" 
                          actifSiFormulaireLectureSeulement="true" 
                          libelle="${libelle_retour}" 
                          activerConfirmationModificationFormulaire="true" />
      </c:when>
      <c:otherwise>
        <sofi-html:bouton styleId="btRetour" 
                          href="${url_retour}" 
                          actifSiFormulaireLectureSeulement="true" 
                          libelle="${libelle_retour}" 
                          activerConfirmationModificationFormulaire="true" />
      </c:otherwise>
    </c:choose>
  </c:if>
  <c:if test="${libelle_ajouter ne null && libelle_ajouter ne ''}">  
    <sofi-html:bouton href="${url_ajouter}" 
                      libelle="${libelle_ajouter}" 
                      disabled="${disabled_condition || (disabled_condition_nouveau ne null && disabled_condition_nouveau)}"
                      activerConfirmationModificationFormulaire="true" />
  </c:if>
  <c:if test="${libelle_supprimer ne null && libelle_supprimer ne ''}">
    <c:choose>
      <c:when test="${message_suppression ne null && message_suppression ne ''}">
        <sofi-html:bouton href="${url_supprimer}" 
                          messageConfirmation="${message_suppression}"  
                          libelle="${libelle_supprimer}" 
                          disabled="${disabled_condition || (disabled_condition_supprimer ne null && disabled_condition_supprimer)}" />
      </c:when>
      <c:otherwise>
        <sofi-html:bouton href="${url_supprimer}" 
                          messageConfirmation="eregroupement.avertissement.suppression.enfant"  
                          libelle="${libelle_supprimer}" 
                          disabled="${disabled_condition || (disabled_condition_supprimer ne null && disabled_condition_supprimer)}" />
      </c:otherwise>
    </c:choose>
  </c:if>
</div>