<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ attribute name="libelle" required="true" type="java.lang.String"%>
<%@ attribute name="urlSoumettre" required="false" type="java.lang.String"%>
<%@ attribute name="urlListe" required="true" type="java.lang.String"%>
<%@ attribute name="divListe" required="true" type="java.lang.String"%>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean"%>
<%@ attribute name="notifierFormulaireParent" required="false" type="java.lang.Boolean"%>
<%@ attribute name="formulaireParent" required="false" type="java.lang.String"%>

<c:set var="jsSoumission" value="
  var fonction = function() {
    ajaxUpdate('${urlListe}', '${divListe}');
  };
  var formulaire = $(this).parents('form');
  formulaire.attr('action', '${urlSoumettre}');
  postDialog(this, fonction);
  inactiverBoutonsFormulaire(formulaire);"
/>

<c:choose>
  <c:when test="${notifierFormulaireParent}">
    <sofi-html:bouton onclick="notifierModificationFormulaire('${formulaireParent}');${jsSoumission}" 
                      libelle="${libelle}" 
                      disabled="${disabled}"
                      activerConfirmationModificationFormulaire="true" />
  </c:when>
  <c:otherwise>
    <sofi-html:bouton onclick="${jsSoumission}" 
                      libelle="${libelle}" 
                      disabled="${disabled}"
                      activerConfirmationModificationFormulaire="true" />
  </c:otherwise>
</c:choose>