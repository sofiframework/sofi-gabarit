<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<br/>
<c:if test="${documentPdf != null }">
  <c:url value="edocument.do?method=download&type=documentPdf" var="edocLink"></c:url> 
</c:if>
<c:if test="${documentCsv != null }">
  <c:url value="edocument.do?method=download&type=documentCsv" var="edocLink"></c:url>
</c:if>

<script>
  function downloadEdoc(){
  window.location.href = '<c:out escapeXml="false" value="${edocLink}" />';
  }
</script>