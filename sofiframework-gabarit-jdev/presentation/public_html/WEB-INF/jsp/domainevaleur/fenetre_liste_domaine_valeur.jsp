<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<sofi-liste:listeNavigation
    action="listeDomaineValeur.do?methode=rechercher"
    nomListe="${listeDomaineValeur}"
    trierPageSeulement="false"
    triDefaut="1"
    class="tableresultat" 
    iterateurMultiPage="true"
    libelleResultat="infra_sofi.libelle.commun.liste_navigation.compteur.element"
    ajax="true"
    fonctionRetourValeurAjax="true" >
    <sofi-liste:colonne property="description"
        trier="true"
        fonctionRetourValeurAjax="true"
        parametresFonction="valeur, description"
        libelle="eregroupement.libelle.liste_domaine_valeur.colonne.description" />
</sofi-liste:listeNavigation>