<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-tiles" prefix="sofi-tiles"%>

<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="app-dialog" tagdir="/WEB-INF/tags/dialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true">
<head>
  <tiles:importAttribute name="titreApplication" />
  <tiles:importAttribute name="titre" ignore="true" />
  <sofi:libelle var="titrePage" identifiant="${titreApplication}" />
  <c:if test="${titre != null}">
    <sofi:libelle var="titre" identifiant="${titre}" />
    <c:set var="titrePage" value="${titrePage} -> ${titre}" />
  </c:if>
  <title><c:out value="${titrePage}"/></title> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <sofi:css href="wro/gabarit.css"></sofi:css>
  <!--[if IE 7]>
    <sofi:css href="css/ie7.css"></sofi:css>
  <![endif]-->
  <sofi:javascript src="wro/gabarit.js"></sofi:javascript>
  <!-- afficher le calendrier dans la langue de l'utilisateur -->
  <c:if test="${(codeLangueEnCours != null) && (codeLangueEnCours == 'en_US')}">
    <sofi:javascript src="script/calendrier/lang/calendar-en.js"></sofi:javascript>
  </c:if>
  <c:if test="${(codeLangueEnCours == null) || (codeLangueEnCours == 'fr_CA')}">
    <sofi:javascript src="script/calendrier/lang/calendar-ca-fr.js"></sofi:javascript>
  </c:if>
</head>
<tiles:useAttribute name="menu" ignore="true" />
<tiles:useAttribute name="filnavigation" ignore="true" />
<tiles:useAttribute name="onglet" ignore="true" />
<tiles:useAttribute name="sommaire" ignore="true" />
<tiles:useAttribute name="document_electronique" ignore="true" />
<body>
  <div id="background">
   <div id="contenant">
  	  <tiles:insert attribute="entete"/>
      <app:listeLangueSupportee libelle="login.libelle.commun.liste_langue" 
                                 styleClass="listeLangue"/>
      <c:if test="${menu != null}">
        <tiles:insert attribute="menu"/> 
      </c:if>
      <c:if test="${filnavigation != null}">
        <div id="zone_fond_navigation">
          <c:if test="${filnavigation != null}" >
            <tiles:insert attribute="filnavigation"/>
          </c:if>
      </c:if>
      <c:if test="${sommaire != null}"> 
        <tiles:insert attribute="sommaire"/>
      </c:if>
      <c:if test="${onglet != null}">
 		    <tiles:insert attribute="onglet"/>
      </c:if>
      
      <sofi:barreStatut afficherSeulementSiMessage="false"/>

      <c:if test="${document_electronique != null}"> 
        <tiles:insert attribute="document_electronique"/>
      </c:if>

      <div id="corps">
        <tiles:insert attribute="detail"></tiles:insert>
      </div>

      <div class="clear"></div>

      <div id="bas">
        <tiles:insert attribute="bas_page"/>
      </div>
    </div>
  </div>

  <app:fenetreAttente styleId="attenteAjax" libelle="En traitement, veuillez attentre un moment svp"></app:fenetreAttente>

</body>
<sofi:javascript appliquerOnLoad="true" />
</html:html>