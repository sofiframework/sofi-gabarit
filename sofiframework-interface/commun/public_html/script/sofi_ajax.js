var xmlhttp=false;
var debugAjax = false;

var urlSessionTimeOut = 'accueil.do?methode=acceder&sessionTimeOut=true';
//var urlSessionTimeOut = 'home.do?method=access&sessionTimeOut=true';

function forcerEvenementHTML(obj) {
  
  var eventType = "onblur"; 
  if(typeof obj.fireEvent != "undefined"){
    obj.fireEvent(/^on/.test(eventType) ? eventType : "on" + eventType)
  }
  else {
    obj.focus();

    var evt = document.createEvent("HTMLEvents");
    evt.initEvent(eventType.replace(/^on/,""),true,true)

    obj.dispatchEvent(evt);
  }
}

function creerXMLHttpRequest() {
  /*@cc_on @*/
  /*@if (@_jscript_version >= 5)
   try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
   } catch (e) {
    try {
     xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
     xmlhttp = false;
    }
   }
  @end @*/
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
}

function creerXMLHttpRequestAsynchrone() {
  /*@cc_on @*/
  /*@if (@_jscript_version >= 5)
   try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
   } catch (e) {
    try {
     xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
     xmlhttp = false;
    }
   }
  @end @*/
  var xmlhttp = false;
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}

function traiterRequeteGETASynchrone(url) {
/*  creerXMLHttpRequest();
  xmlhttp.open('GET', url , true);
  xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState==4) {
   if (xmlhttp.status!=200) {
   }
  }
 }
 xmlhttp.send(null);*/
 $.get(url);
}

function traiterRequeteGET(url) {
  /*
  creerXMLHttpRequest();
  xmlhttp.open('GET', url , false);
  xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState==4) {
   if (xmlhttp.status!=200) {
   }
  }
 }
 xmlhttp.send(null);*/
 $.get(url);
}

function traiterRequeteGETAvecReponseHTML(url) {
  creerXMLHttpRequest();
  xmlhttp.open('GET', url ,false);
  xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState==4) {
   if (xmlhttp.status!=200) {
    return "erreur500";
   }
  }
 }
  xmlhttp.send(null);
  if (xmlhttp.status!=200) {
    return "erreur500";
  }
  return xmlhttp.responseText;
}
var requeteAjax = new Array();

function traiterAffichageDivAjaxAsynchroneAvecMessage(baseUrl, idElement, urlMessage, idDivMessage, fonctionPendantChargement, fonctionApresChargement, tempRafraichissement) {
  var indiceCourant = requeteAjax.length;

  if (urlMessage != null) {
    fonctionApresChargementTmp = 'traiterAffichageDivAjaxAsynchroneAvecMessage(\''+urlMessage + '\',\'' + idDivMessage + '\');';
    if (fonctionApresChargement != "null" && fonctionApresChargement != "undefined") {
      fonctionApresChargementTmp += fonctionApresChargement;
    }
    fonctionApresChargement = fonctionApresChargementTmp;
  }
  
  traiterAffichageDivAjaxAsynchrone(baseUrl, idElement, fonctionPendantChargement,fonctionApresChargement, null, false );
}

function traiterAffichageDivAjaxAsynchrone(baseUrl, idElement, fonctionPendantChargement, fonctionApresChargement, tempRafraichissement, synchrone, divDynamiqueOuvert) {

  if (fonctionApresChargement && fonctionApresChargement != "null" && fonctionApresChargement != "undefined") {
    fonctionApresChargement = fonctionApresChargement + ';NiceTitles.autoCreation();';
  }else {
    fonctionApresChargement = 'NiceTitles.autoCreation();';
  }
  var indexRequeteAjax = requeteAjax.length;
  requeteAjax[indexRequeteAjax] = new sack();
  requeteAjax[indexRequeteAjax].requestFile = baseUrl;
  requeteAjax[indexRequeteAjax].method='GET';
  requeteAjax[indexRequeteAjax].element = idElement;  

  if (synchrone) {
   requeteAjax[indexRequeteAjax].asynchrone = false;
  }

  requeteAjax[indexRequeteAjax].onLoading = function() {

    if (fonctionPendantChargement != "null" && fonctionPendantChargement != "undefined") {
    	
      eval(fonctionPendantChargement);
    }
  };
  
  requeteAjax[indexRequeteAjax].onCompletion = function(){ 
    if (fonctionApresChargement != "null" && fonctionApresChargement != "undefined") {
      if ((requeteAjax[indexRequeteAjax].response.indexOf('session') != -1) && idElement != 'null' || (!IE && fonctionApresChargement.indexOf('window.close') != -1) ) {
    	if (urlSessionTimeOut != '') {
			window.location = urlSessionTimeOut;
		}
      }else {
        eval(fonctionApresChargement);
      }
    }
  };
  
  if (tempRafraichissement != null && tempRafraichissement != "undefined") {
    requeteAjax[indexRequeteAjax].setTempsRafraichissement(tempRafraichissement);
    appelRecursifChargement(requeteAjax[indexRequeteAjax], divDynamiqueOuvert);
  }else {
    requeteAjax[indexRequeteAjax].runAJAX();
  }
}

function appelRecursifChargement(instanceAjax, divDynamiqueOuvert) {
  var tempEnSeconde = instanceAjax.getTempsRafraichissement() * 1000;
  if (instanceAjax.getNbAppel() && instanceAjax.getNbAppel() != 1) {
    if (sofiChargementPageComplete != 'false') {
     instanceAjax.runAJAX();
    }
  }else {
    instanceAjax.setNbAppel(2);
    if (!divDynamiqueOuvert) {
      setTimeout(function() { appelRecursifChargement(instanceAjax, divDynamiqueOuvert); }, 0.01);
    }
  }
  setTimeout(function() { appelRecursifChargement(instanceAjax, divDynamiqueOuvert); }, tempEnSeconde);
}

function traiterListeDeroulantAjaxAsynchrone(baseUrl, idListe) {
  var xmlhttp = creerXMLHttpRequestAsynchrone();
  xmlhttp.open('GET', baseUrl);
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4) {
      ajouterLigneDebug('Statut de la réponse -> ' + xmlhttp.status + ' ' + xmlhttp.statusText);
      if (xmlhttp.status == 200) {
        var liste = document.getElementById(idListe);

        if (liste == null) {
        }

        // Initialiser la liste
        liste.options.length = 0;
        liste.disabled = false;
        if (xmlhttp.responseXML) {
          // Extraire la source Xml.
          var sourceXml = xmlhttp.responseXML.documentElement;
          if (sourceXml != null) {
            // Remplir la liste déroulante.
            var items = sourceXml.getElementsByTagName("item");
            for (var i=0; i<items.length; i++) {
              var nom = ""; 
              var valeur = "";
              try {
                nom = items[i].getElementsByTagName("nom")[0].firstChild.nodeValue;
                valeur = items[i].getElementsByTagName("valeur")[0].firstChild.nodeValue;
                
                if (debugAjax) {
                  ajouterLigneDebug(nom + ' ' + valeur);
                }
              } catch (E) {
              }
              liste.options[i] = new Option(nom, valeur);
            }

            // Ajout pour traiter le changement de className et de disable de champs aux retour ajax.
            try{
              // Changer les proprietes demandés
              var disabled = sourceXml.getElementsByTagName("disabled")[0].firstChild.nodeValue;
              if (disabled != null){
                if (disabled == 'true' || disabled == true){
                   liste.disabled = true;               
               
                } else {                
                   liste.disabled = false;
                }
              }
              var classname = sourceXml.getElementsByTagName("classname")[0].firstChild.nodeValue;
              liste.className = classname;               
          
            } catch (E) {
            }
          }
        }

      }else {
        if (debugAjax) {
          ajouterLigneDebug('Réponse XML -> ' + xmlhttp.responseXML);
        }
      }
      
      if (debugAjax) {
        afficherFenetreDebug();
      }
    }
  }

  xmlhttp.send(null);
}

function sack(file) {
  this.xmlhttp = null;
  this.tempRafraichissement = null;
  this.nbAppel = 1;

  this.resetData = function() {
    this.method = "POST";
    this.queryStringSeparator = "?";
    this.argumentSeparator = "&";
    this.URLString = "";
    this.encodeURIString = true;
    this.execute = false;
    this.element = null;
    this.elementObj = null;
    this.requestFile = file;
    this.vars = new Object();
    this.responseStatus = new Array(2);
    this.asynchrone = true;
    };

  this.resetFunctions = function() {
    this.onLoading = function() { };
    this.onLoaded = function() { };
    this.onInteractive = function() { };
    this.onCompletion = function() { 
    };
    this.onError = function() { };
    this.onFail = function() { };
  };

  this.reset = function() {
    this.resetFunctions();
    this.resetData();
  };

  this.createAJAX = function() {
    if (! this.xmlhttp) {
      if (typeof XMLHttpRequest != "undefined") {
        this.xmlhttp = new XMLHttpRequest();
      } else {
        this.failed = true;
      }
    }
  };

  this.setTempsRafraichissement = function(temps) {
    this.tempRafraichissement = temps;              
  };

  this.getTempsRafraichissement = function() {
    return this.tempRafraichissement;
  }
  
  this.setNbAppel = function(nbAppel) {
    this.nbAppel = nbAppel;              
  };

  this.getNbAppel = function() {
    return this.nbAppel;
  }

  this.setVar = function(name, value){
    this.vars[name] = Array(value, false);
  };

  this.isExecute = function() {
    return this.execute;
  } 

  this.setExecute = function(executeParam) {
    this.execute = executeParam;
  }   

  this.encVar = function(name, value, returnvars) {
    if (true == returnvars) {
      return Array(encodeURIComponent(name), encodeURIComponent(value));
    } else {
      this.vars[encodeURIComponent(name)] = Array(encodeURIComponent(value), true);
    }
  }

  this.processURLString = function(string, encode) {
    encoded = encodeURIComponent(this.argumentSeparator);
    regexp = new RegExp(this.argumentSeparator + "|" + encoded);
    varArray = string.split(regexp);
    for (i = 0; i < varArray.length; i++){
      urlVars = varArray[i].split("=");
      if (true == encode){
        this.encVar(urlVars[0], urlVars[1]);
      } else {
        this.setVar(urlVars[0], urlVars[1]);
      }
    }
  }

  this.createURLString = function(urlstring) {
    if (this.encodeURIString && this.URLString.length) {
      this.processURLString(this.URLString, true);
    }

    if (urlstring) {
      if (this.URLString.length) {
        this.URLString += this.argumentSeparator + urlstring;
      } else {
        this.URLString = urlstring;
      }
    }

    urlstringtemp = new Array();
    for (key in this.vars) {
      if (false == this.vars[key][1] && true == this.encodeURIString) {
        encoded = this.encVar(key, this.vars[key][0], true);
        delete this.vars[key];
        this.vars[encoded[0]] = Array(encoded[1], true);
        key = encoded[0];
      }

      urlstringtemp[urlstringtemp.length] = key + "=" + this.vars[key][0];
    }
    if (urlstring){
      this.URLString += this.argumentSeparator + urlstringtemp.join(this.argumentSeparator);
    } else {
      this.URLString += urlstringtemp.join(this.argumentSeparator);
    }
  }

  this.runResponse = function() {
    eval(this.response);
  }

  this.runAJAX = function(urlstring) {
    var elementAjaxMultiple = false;
    var liste = null;

    // Indicateur si element ajax multiple dans la réponse.
    if (this.element == 'REPONSE_MULTIPLE') {
      elementAjaxMultiple = true;
    }

    if (this.failed) {
      this.onFail();
    } else {
      this.createURLString(urlstring);
      if (this.element && !elementAjaxMultiple) {
        this.elementObj = document.getElementById(this.element);
      }
      
      if (this.elementObj && this.elementObj.options) {
        // Fixer la liste déroulante a remplir.
        liste = this.elementObj;
      }

      if (this.xmlhttp) {
        var self = this;

        if (this.method == "GET") {
          totalurlstring = this.requestFile + this.URLString;
          this.xmlhttp.open(this.method, totalurlstring, this.asynchrone);
        } else {
          this.xmlhttp.open(this.method, this.requestFile, true);
          try {
            this.xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
          } catch (e) { }
        }

        this.xmlhttp.onreadystatechange = function() {
          switch (self.xmlhttp.readyState) {
            case 1:
              self.onLoading();
              break;
            case 2:
              self.onLoaded();
              break;
            case 3:
              self.onInteractive();
              break;
            case 4:
              self.response = self.xmlhttp.responseText;
              self.responseXML = self.xmlhttp.responseXML;
              self.responseStatus[0] = self.xmlhttp.status;
              self.responseStatus[1] = self.xmlhttp.statusText;
              
              if (self.response.indexOf("reponse-ajax") != -1) {
                elementAjaxMultiple = true;
              }
              
              
               if (self.responseStatus[0] != '500') { 
                try {
                  if (self.execute) {
                    self.runResponse();
                  }
                } catch (e) {
                  self.execute = false;
                }
                self.execute = true;

                if (elementAjaxMultiple == false && (liste == null || self.xmlhttp.responseXML.documentElement == null)) {
                	
	            	try {
	            		this.elementObj = $('#' + self.element);
	            	} catch (E) {
	            		this.elementObj = document.getElementById(self.element);
	            	}
	            	
	    

                  if (self.elementObj) {
                    elemNodeName = self.elementObj.nodeName;
                    elemNodeName = elemNodeName.toLowerCase();
                   
                    if (elemNodeName == "input"
                    || elemNodeName == "select"
                    || elemNodeName == "option"
                    || elemNodeName == "textarea") {
                      self.elementObj.value = self.response;
                    } else {
                      if (self.response != '' || (self.response == '' && self.elementObj.innerHTML != '')) {
                        if (totalurlstring.indexOf("sofi_eclatable=true") != -1) {
                          if (self.elementObj.innerHTML.length == 5 || self.elementObj.innerHTML.length == 0 || self.elementObj.innerHTML.length == 1) {
                            // correction d'un bug avec IE7 pour le remplacement d'un div
                            $(self.elementObj).html(self.response);
                            //self.elementObj.innerHTML = self.response;
                          }else {
                            $(self.elementObj).empty();
                            //self.elementObj.innerHTML = '';
                          }
                        }else {
                          // correction d'un bug avec IE7 pour le remplacement d'un div
                          $(self.elementObj).html(self.response);
                          //self.elementObj.innerHTML = self.response;
                        }
                      }else {
                        $(self.elementObj).empty();
                        //self.elementObj.innerHTML = '';
                      }
                    }
                  }
                }else {
                  if (liste == null) {
                    // Traitement pour plusieurs valeur recu en XML.
                    
                    // Extraire la source XML.
                    var sourceXml = self.xmlhttp.responseXML.documentElement;
                    
                    // Accéder aux éléments de réponse.
                    var items = sourceXml.getElementsByTagName("item");
  
                    // Itérer chacun des éléments de réponse.
                    for (var i=0; i<items.length; i++) {
                      var nom = ""; 
                      var valeur = "";
                      try {
                      
                        nom = items[i].getElementsByTagName("nom")[0].firstChild.nodeValue;
                        
                        element = document.getElementById(nom);
                        
                        elemNodeName = element.nodeName.toLowerCase();

                        valeur = items[i].getElementsByTagName("valeur")[0].firstChild.nodeValue;

                        if (elemNodeName == "input"
                                        || elemNodeName == "select"
                                        || elemNodeName == "option"
                                        || elemNodeName == "textarea") {
                            element.value = valeur;
                        }else {
                           element.innerHTML = valeur;
                        }                     
                        ajouterLigneDebug("Element : " + nom + " Valeur : " + valeur);
                      } catch (E) {
                        // Si l'erreur se produit sur la récupération de la valeur de 'nom', element est null, on a donc une erreur dans le catch
//                        if (element) {
//                          element.value = "";
//                        }
                      }
                    }
                  } else {
                    if (self.xmlhttp.responseXML != null && self.xmlhttp.responseXML.documentElement != null) {

                      // Extraire la source XML.
                      var sourceXml = self.xmlhttp.responseXML.documentElement;    

                      // Initialiser la liste
                      liste.options.length = 0;
                      liste.disabled = false;
                      
                      // Remplir la liste déroulante.
                      var items = sourceXml.getElementsByTagName("item");
                      
                      for (var i=0; i<items.length; i++) {
                        var nom = ""; 
                        var valeur = "";
                        try {
                          nom = items[i].getElementsByTagName("nom")[0].firstChild.nodeValue;
                          valeur = items[i].getElementsByTagName("valeur")[0].firstChild.nodeValue;
                          
                          if (debugAjax) {
                            ajouterLigneDebug(nom + ' ' + valeur);
                          }
                        } catch (E) {
                        }
                        liste.options[i] = new Option(nom, valeur);
                      }
                      
                      // Ajout pour traiter le changement de className et de disable de champs aux retour ajax.
                      try{
                        // Changer les proprietes demandés
                        var disabled = sourceXml.getElementsByTagName("disabled")[0].firstChild.nodeValue;
                        if (disabled != null){
                          if (disabled == 'true' || disabled == true){
                             liste.disabled = true;
                         
                          } else {                
                             liste.disabled = false;
                          }
                        }
                        var classname = sourceXml.getElementsByTagName("classname")[0].firstChild.nodeValue;
                        liste.className = classname;
                      
                      } catch (E) {
                      }
                    }
                  }
                }
              if (self.responseStatus[0] == "200") {
                NiceTitles.autoCreation();
                self.onCompletion();
                
                if (debugAjax) {
                  ajouterLigneDebug('Statut de la réponse -> ' + self.responseStatus[0] + ' ' + self.responseStatus[1]);
                  ajouterLigneDebug(self.response);
                }                

              } else {
                self.onError();

                if (debugAjax) {
                  ajouterLigneDebug('Statut de la réponse -> ' + self.responseStatus[0] + ' ' + self.responseStatus[1]);
                  ajouterLigneDebug(self.response);
                }
              }

              if (debugAjax) {
                afficherFenetreDebug();
              }

              execute=true;
              self.URLString = "";
              break;
            }
          }
        };
        
        this.xmlhttp.send(this.URLString);


      }
    }
  };

  this.reset();
  this.createAJAX();
}

function setStyleClassSectionEclatable(id, divRetour, styleClassOuvert, styleClassFermer) {
  try {
    var element = document.getElementById(divRetour);

    var styleCourant = document.getElementById(id).className;

    if (styleCourant == styleClassOuvert) {
      setStyleClass(id, styleClassFermer);
    }else {
      setStyleClass(id, styleClassOuvert);
    }
  } catch (E) {}
}

function setDebugAjax(debug) {
  debugAjax=debug;
  try {
    var args='width=800,height=600,left=1,top=1,toolbar=0,';
       args+='location=0,status=0,menubar=0,scrollbars=1,resizable=1';
    var popupDebug = window.open("","popupSOFIDebug", args);
  } catch (E) {}
}

