<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles"  prefix="tiles"%>
<tiles:importAttribute name="titreApplication" />
<c:if test="${serviceSelectionnee.type == 'SE'}" var="typeService">
  <c:set var="titreServiceEnCours" value="${serviceSelectionnee.nom}" />
  <sofi:libelle identifiant="${titreServiceEnCours}" var="libelleTitreService" iconeAide="false" />
</c:if>
<c:if test="${!typeService}" >
  <c:set var="titreServiceEnCours" value="${serviceSelectionneeParent.nom}" />
  <c:set var="titreOngletEnCours" value="${serviceSelectionnee.nom}" />
  <sofi:libelle identifiant="${titreServiceEnCours}" var="libelleTitreService" iconeAide="false" />
  <sofi:libelle identifiant="${titreOngletEnCours}" var="libelleTitreOnglet" iconeAide="false" />
</c:if>
<c:set var="ongletEgaleService" value="${libelleTitreService == libelleTitreOnglet}" />
<c:if test="${(typeService || ongletEgaleService) && libelleTitreService != ''}" >
  <c:set var="titrePage" value="${titreApplication} -> ${libelleTitreService}"/>  
</c:if>
<c:if test="${(!typeService && !ongletEgaleService) && libelleTitreService != '' }" > 
  <c:set var="titrePage" value="${titreApplication} -> ${libelleTitreService} -> ${libelleTitreOnglet}"/>  
</c:if> 
<c:if test="${titrePage == null}" > 
  <c:set var="titrePage" value="${titreApplication} -> ${libelleTitreOnglet}"/>  
</c:if>
${titrePage}