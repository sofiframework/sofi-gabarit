<%@ tag body-content="scriptless" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>

<%@ attribute name="nomProprieteIdentifiant" required="true" type="java.lang.String"%>
<%@ attribute name="nomProprieteDescription" required="true" type="java.lang.String"%>
<%@ attribute name="url" required="true" type="java.lang.String"%>
<%@ attribute name="nested" required="false" type="java.lang.Boolean"%>
<%@ attribute name="maxlength" required="false" type="java.lang.String"%>
<%@ attribute name="size" required="false" type="java.lang.String"%>
<%@ attribute name="obligatoire" required="false" type="java.lang.Boolean"%>
<%@ attribute name="styleClass" required="false" type="java.lang.String"%>

<%@ attribute name="onchangeIdentifiant" required="false" type="java.lang.String"%>
<%@ attribute name="onchangeDescription" required="false" type="java.lang.String"%>

<c:set value="${nomProprieteDescription}" var="proprieteDescription" />

<c:choose>
  <c:when test="${nested}">
    <sofi-nested:text property="${proprieteDescription}" 
                      onfocus="initialiserAutoComplete('${nomProprieteIdentifiant}', '${proprieteDescription}', '${url}', this);"
                      onchange="if($(this).val() == '') {var champ = getNomNested('${nomProprieteIdentifiant}', this);$('#' + champ).val('');}${onchangeDescription}" 
                      size="${size}" maxlength="${maxlength}" obligatoire="${obligatoire}"
                      styleClass="${styleClass}"
                      autocomplete="off" />
    <div style="display:none">
      <sofi-nested:text property="${nomProprieteIdentifiant}" onchange="${onchangeIdentifiant}" />
    </div>
  </c:when>
  <c:otherwise>
    <sofi-html:text property="${proprieteDescription}" 
                    onfocus="initialiserAutoComplete('${nomProprieteIdentifiant}', '${proprieteDescription}', '${url}', this);"
                    onchange="if($(this).val() == '') {var champ = '${nomProprieteIdentifiant}';$('#' + champ).val('');}${onchangeDescription}" 
                    size="${size}" maxlength="${maxlength}" obligatoire="${obligatoire}"
                    styleClass="${styleClass}"
                    autocomplete="off" />
    <div style="display:none">
      <sofi-html:text property="${nomProprieteIdentifiant}" onchange="${onchangeIdentifiant}" />
    </div>
  </c:otherwise>
</c:choose>