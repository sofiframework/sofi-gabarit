<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<fieldset>
  <legend><sofi:libelle identifiant="infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.titre" /></legend>
    <jsp:include page="ajax_liste_utilisateurs_actif.jsp" />
</fieldset>