<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<style>
  ul {
    padding:1px;
    margin-top:2px;
    margin-left:15px;
    margin-bottom:2px;    
  }
</style>
<fieldset>
  <legend><sofi:libelle identifiant="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.titre" /></legend>
  <table class="tableaffichage">
    <sofi:affichageEL valeur="${utilisateurDetail.nom}" 
                      libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.nom"/>
    <sofi:affichageEL valeur="${utilisateurDetail.prenom}" 
                      libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.prenom"/>
                      
<%--                      
    <c:if test="${utilisateurDetail.codeUtilisateurApplicatif != null}">
      <sofi:affichageEL valeur="${utilisateurDetail.codeUtilisateurApplicatif}" 
                        libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.codeUtilisateur">
                        (<c:out value="${utilisateurDetail.codeUtilisateur}" />)
      </sofi:affichageEL>
    </c:if>
--%>    
    <c:if test="${utilisateurDetail.codeUtilisateurApplicatif == null}">
      <sofi:affichageEL valeur="${utilisateurDetail.codeUtilisateur}" 
                        libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.codeUtilisateur"/>
    </c:if>    
    <sofi:affichageEL libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.role">
      <ul>
        <c:forEach items="${listeRoles}" var="role">
          <li>
            <c:out value="${role.nom}" /> (<c:out value="${role.code}" />)
            <c:catch >
              <c:if test="${role.listeClientDetaille != null}" >
                <ul>
                 <c:forEach items="${role.listeClientDetaille}" var="client"> 
                    <li>
                      <c:out value="${client.nom}" />&nbsp;
                      
                      <c:if test="${client.nomCourt != null}" >
                        (<c:out value="${client.nomCourt}" />)
                      </c:if>
                    </li>
                 </c:forEach>
                </ul>
              </c:if>
            </c:catch>
          </li>
        </c:forEach>
      </ul>
    </sofi:affichageEL>
    <sofi:affichageEL valeur="${utilisateurSession.adresseIP}" 
                      libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.adresseIP"/>
    <sofi:affichageEL valeur="${utilisateurSession.navigateur}" 
                      libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.navigateur"/>
    <sofi:affichageEL valeur="${utilisateurSession.creationAcces}" 
                      libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.cree_acces"/>
    <sofi:affichageEL valeur="${utilisateurSession.dernierAcces}" 
                      libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.dernier_acces"/>
    <sofi:affichageEL valeur="${utilisateurSession.derniereAction}" 
                      libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.derniere_action"/>
  </table>
</fieldset>


