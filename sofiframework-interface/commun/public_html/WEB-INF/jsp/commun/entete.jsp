<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>

<div id="logo_general">
  <div id="bouton_accueil"><a href="index.html" /></div>
  <sofi-html:link styleId="fermerSession" styleClass="blanc"
    href="authentification.do?methode=quitter"
    libelle="infra_sofi.libelle.commun.fermer_session"
    messageConfirmation="infra_sofi.confirmation.commun.fermer_session" />
  <div id="zone_utilisateur">
    <p class="utilisateur"><sofi:libelle identifiant="infra_sofi.libelle.commun.bienvenue" /> <span> <c:out
      value="${utilisateur.prenom}" />&nbsp;<c:out value="${utilisateur.nom}" />
    </span></p>
    <ul>
      <li class="btn_aide"><sofi:aide styleId="aideGeneral" ajax="true"
        styleClass="aideEnLigne" largeurfenetre="655" hauteurfenetre="400">
        <sofi:libelle identifiant="infra_sofi.libelle.commun.aide_en_ligne" />
      </sofi:aide></li>
      <li class="btn_nbUtilisateur"><sofi:popup
        href="surveillance.do?methode=afficherUtilisateurEntete" ajax="true"
        largeurfenetre="250" hauteurfenetre="150" ajustementPositionX="-220"
        ajustementPositionY="30" styleId="fenetreNbUtilisateur">
        <span id="idNbUtilisateurEnLigne"> <c:out
          value="${nbUtilisateurEnLigne}" /> </span>
      </sofi:popup></li>
    </ul>
  </div>
</div>

<sofi:div id="zoneRafrachissementUtilisateurEntete"
  href="surveillance.do?methode=rafraichirUtilisateurEntete"
  tempsRafraichissement="5" />

<div class="clear"></div>