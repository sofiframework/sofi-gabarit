<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<br/>
<c:url value="documentPdf.do?methode=telechargerPdf" var="lienPdf"></c:url>
<script>
  function telechargement(){
  display('message_erreur', false);
  display('div_document_pdf', false); 
  window.location.href = '<c:out value="${lienPdf}" />';
  }
</script>
<div id="div_document_pdf" align="right" style="margin-right:15px;margin-top:-15px">
  
  <img src="images/commun/icone_pdf.gif"/>&nbsp;
  <a href="#" onclick="telechargement()" >
    <sofi:parametreSysteme code="cleLibelleTelechargerPdf"
                           var="cleLibelleTelechargement"/>
    <sofi:libelle identifiant="${cleLibelleTelechargement}"/>
  </a>
  (<c:out value="${documentPdf.volumeFichierEnKoAffichage}"/>)
</div>
